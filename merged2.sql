--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: vintagedesign; Type: DATABASE; Schema: -; Owner: postgres
--

DROP DATABASE IF EXISTS vintagedesign 

CREATE DATABASE vintagedesign WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE vintagedesign OWNER TO postgres;

\connect vintagedesign

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: articles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE articles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    text character varying(5000) NOT NULL,
    "mainPhoto" character varying(255) NOT NULL,
    photos character varying(2000) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE articles OWNER TO postgres;

--
-- Name: articles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE articles_id_seq OWNER TO postgres;

--
-- Name: articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE articles_id_seq OWNED BY articles.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categories (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(10000) NOT NULL,
    photo character varying(500) NOT NULL
);


ALTER TABLE categories OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: mirrors; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE mirrors (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    photos character varying(10000) NOT NULL,
    price integer NOT NULL
);


ALTER TABLE mirrors OWNER TO postgres;

--
-- Name: mirrors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE mirrors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mirrors_id_seq OWNER TO postgres;

--
-- Name: mirrors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE mirrors_id_seq OWNED BY mirrors.id;


--
-- Name: opinions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE opinions (
    name character varying(255) NOT NULL,
    photo character varying(1000) NOT NULL,
    text character varying(1000) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE opinions OWNER TO postgres;

--
-- Name: opinions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE opinions_id_seq
    START WITH 9
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE opinions_id_seq OWNER TO postgres;

--
-- Name: opinions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE opinions_id_seq OWNED BY opinions.id;


--
-- Name: orderMirrors; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "orderMirrors" (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    photo character varying(500) NOT NULL,
    category character varying(255) NOT NULL
);


ALTER TABLE "orderMirrors" OWNER TO postgres;

--
-- Name: orderMirrors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "orderMirrors_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "orderMirrors_id_seq" OWNER TO postgres;

--
-- Name: orderMirrors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "orderMirrors_id_seq" OWNED BY "orderMirrors".id;


--
-- Name: play_evolutions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE play_evolutions (
    id integer NOT NULL,
    hash character varying(255) NOT NULL,
    applied_at timestamp(6) without time zone NOT NULL,
    apply_script text,
    revert_script text,
    state character varying(255),
    last_problem text
);


ALTER TABLE play_evolutions OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY articles ALTER COLUMN id SET DEFAULT nextval('articles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mirrors ALTER COLUMN id SET DEFAULT nextval('mirrors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY opinions ALTER COLUMN id SET DEFAULT nextval('opinions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "orderMirrors" ALTER COLUMN id SET DEFAULT nextval('"orderMirrors_id_seq"'::regclass);


--
-- Data for Name: articles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO articles VALUES (1, 'Эгломизе', '<p>Этот вид украшения стекла является одним из самых древних и известен еще с римских времен. Изначально эгломизе представляло собой род гравюры на обратной стороне стекла, покрытой тонким слоем золотой фольги.</p></cut>
<p>На рубеже XVIII-XIX веков французский рисовальщик, гравер, антиквар и писатель Ж.-Б.Гломи возродил и несколько усовершенствовал этот способ и стал создавать силуэтные изображения, прежде всего это были портреты, на золоченом поле стекла.</p>
<p> Уже несколько подзабытая техника получила имя человека, вновь сделавшего ее популярной. Принципиальное различие техник древней эгломизы и наших дней в том, что сегодня на узорах хорошо видна кристаллическая структура изображения. Как будто в стекле застыли маленькие гранулы золота, они даже имеют собственный объем. </p>
<p>Однако внешняя сторона стекла остается плоской, все изображения на изделиях находятся на глубине и прекрасно защищены от внешних воздействий, то есть, изделия можно протирать, мыть средствами по очистке стекол - с орнаментом ничего не случится.В настоящее время Эгломизе, одна из красивейших техник декорирования стекла и редкий гость в витражных мастерских.</p>', 'p1i1.jpg', '{"photos":["p1i1.jpg","p1i2.jpg","p1i3.jpg","p1i4.jpg","p1i5.jpg","p1i6.jpg","p1i7.jpg"]}');
INSERT INTO articles VALUES (2, 'История происхождения зеркала', '<p>Первые небольшие зеркала, изготовленные из олова, платины и золота, обнаруженные археологами, относятся ещё к Каменному веку. Возраст турецких зеркал из отполированного до блеска обсидиана насчитывает около 7500 лет. В Древнем Риме зеркала из бронзы или стали были большой привилегией, так как уход за металлом, который постоянно окислялся, требовал больших усилий и затрат.</p></cut>
<p>История современного зеркала берет свое начало в Европе, где в 1240 году начали выдувать сосуды из стекла. Позднее, в 1279 году Джон Пекам, используя технологию нанесения тонкого слоя свинца на стекло, изготовил первое настоящее зеркало. Спустя двести лет монополистом в производстве зеркал стала Венеция. 1407 году венецианские братья Данзало дель Галло выкупили у фламандцев патент, и Венеция целых полтора века удерживала монополию на производство отличных венецианских зеркал, которые следовало бы именовать фламандскими. И хотя Венеция была не единственным местом производства зеркал в то время, но именно венецианские зеркала отличало высочайшее качество.</p>
<p> Венецианские мастера добавляли в отражающие составы золото и бронзу, поэтому все предметы в зеркале выглядели даже красивее, чем в действительности. Стоимость одного венецианского зеркала равнялась стоимости небольшого морского судна, и для их покупки французские аристократы иногда были вынуждены продавать целые имения. Например, цифры, дошедшие до наших дней, говорят, что не такое уж большое зеркало размером 100х65 см стоило больше 8000 ливров, а картина Рафаэля того же размера — около 3000 ливров. Зеркала были чрезвычайно дороги.</p>
<p> Покупать и коллекционировать их могли лишь очень богатые аристократы и королевские особы. Ценовой бум был остановлен французами, которые узнав технологию производства стали изготавливать не менее качественные зеркала для дворца Людовика XIV. А в 1835 году в Германии была разработана новая технология с использованием серебра, позволяющая получать более четкое зеркальное отражение.Эта технология, практически без изменений до сих пор используется в производстве зеркал.</p>', 'p2i1.jpg', '{"photos":["p2i1.jpg","p2i2.jpg"]}');
INSERT INTO articles VALUES (3, 'Антиквариат – это всегда дорого, роскошно и великолепно.', '<p> Такие вещи в интерьере всегда привлекали внимание, а старинные зеркала привносят особый налет мистики и загадочности. В таком состоянии зеркало является крайне самостоятельным и значимым акцентом, а помещение приобретает особую ноту роскоши и шарма.</p></cut>', 'p3i1.jpg', '{"photos":["p3i1.jpg","p3i2.jpg"]}');
INSERT INTO articles VALUES (4, 'Зеркало в ванной комнате', '<p>С тем, что любой ванной комнате необходимо хотя бы одно зеркало, не станет спорить ни один домовладелец. Не говоря уже о функциональной стороне вопроса, даже самую скромную обстановку ванной и санузла способно украсить зеркало необычной формы или оригинальная рама для него. Помимо прочего, зеркала способны визуально расширять пространство, что весьма актуально для большинства ванных комнат обычных городских квартир.</p></cut>
<p> Для того, чтобы увеличить пространство, облицовывают всю стену или ее часть зеркалами. Кстати, если сравнить затраты на облицовку одного квадратного метра стены керамической плиткой или зеркалом, то вы будете удивлены результатом – затраты будут вполне соизмеримы. А если вы делаете облицовку стен плиткой премиум класса, то зеркальная стена вам обойдется гораздо дешевле. Вот такая математика. Но так же важно отметить, что декор зеркала должен соответствовать стилю интерьера комнаты. </p>

 <p>Отдельно стоит отметить зеркала с подсветкой, ведь в ванной комнате должно быть достаточно яркое освещение. В случае если помимо центральной лампы или встроенных светильников по периметру комнаты, ваше зеркало подсвечено, то возле него можно будет не только чистить зубы, но и наносить макияж, укладывать волосы и проводить другие процедуры. Наличие нескольких уровней освещения дает вам большую свободу при пользовании ванной комнатой – например, вы можете оставлять лишь подсветку зеркал, когда принимаете ванну для создания романтичной, расслабляющей атмосферы.</p>', 'p4i1.jpg', '{"photos":["p4i1.jpg","p4i2.jpg","p4i3.jpg","p4i4.jpg","p4i5.jpg","p4i6.jpg","p4i7.jpg","p4i8.jpg","p4i9.jpg","p4i10.jpg","p4i11.jpg","p4i12.jpg","p4i13.jpg","p4i14.jpg","p4i15.jpg"]}
');
INSERT INTO articles VALUES (5, 'Зеркало в интерьере', '<p>Зеркало в интерьере отображает особую изюминку вкуса хозяина.</p></cut>
<p>Несколько примеров эффектного применения зеркал в интерьерах.</p>', 'p5i1.jpg', '{"photos":["p5i1.jpg","p5i2.jpg","p5i3.jpg","p5i4.jpg","p5i5.jpg","p5i6.jpg","p5i7.jpg","p5i8.jpg","p5i9.jpg","p5i10.jpg"]}');


--
-- Name: articles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('articles_id_seq', 1, true);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categories VALUES (1, 'Эгломизе', '<p>В этом разделе представлены произведения, выполненные в технике эгломизе. Этот вид украшения стекла является одним из самых древних и известен еще с римских времен.</cut> </p>
									<p>Изначально эгломизе представляло собой род гравюры на обратной стороне стекла, покрытой тонким слоем золотой фольги. На рубеже XVIII-XIX веков французский рисовальщик, гравер, антиквар и писатель Ж.-Б.Гломи возродил и несколько усовершенствовал этот способ и стал создавать силуэтные изображения, прежде всего это были портреты, на золоченом поле стекла. Уже несколько подзабытая техника получила имя человека, вновь сделавшего ее популярной.</p>
									<p>Принципиальное различие техник древней эгломизы и наших дней в том, что сегодня на узорах хорошо видна кристаллическая структура изображения. Как будто в стекле застыли маленькие гранулы золота, они даже имеют собственный объем.</p>
									<p>Однако внешняя сторона стекла остается плоской, все изображения на изделиях находятся на глубине и прекрасно защищены от внешних воздействий, то есть, изделия можно протирать, мыть средствами по очистке стекол - с орнаментом ничего не случится. В настоящее время Эгломизе, одна из красивейших техник декорирования стекла и редкий гость в витражных мастерских.</p>', 'eglomize.jpg');
INSERT INTO categories VALUES (2, 'Состаренное зеркало', '<p>Это зеркало с эффектом потертости, небольших царапин, слегка сошедшей амальгамы. Глядясь в него, создается ощущение, что ты окунулся в прошлое лет 100 назад. Как правило его устанавливают в такой же состаренный багет, либо в такого же вида мебель.</p></cut> 
<p>Прекрасный способ украсить интерьер - использовать зеркало с эффектом состаривания. Каждое такое зеркало выглядит необычайно красиво и получается уникальным, потому что мы делаем их вручную, без использования каких-либо шаблонов и заготовок. Вариантом декоративного оформления может служить сочетание характерных состаренных участков зеркала с трафаретным рисунком. В таком случае зеркало выглядит будто найденным в глубине веков и время давно оставило на нём свои отпечатки. Также благодаря краскам и тонирующим пленкам можно придавать состаренным участкам различные оттенки.</p>
<p>Достигается эффект «потертости временем» с использованием нескольких видов химических травящих средств. Весь процесс создания подобного зеркала довольно трудоемкий, так как требует особой подготовки изделия, соблюдения техники работы и художественного вкуса.</p>
', 'eldering.jpg');
INSERT INTO categories VALUES (3, 'Интерьерные зеркала', '<p>Комбинация различных техник и обрамления зеркал под интерьер помещения. </p></cut>', 'interior.jpg');
INSERT INTO categories VALUES (4, 'Объемная гравировка', '<p> Cоздание на стекле или зеркале объемного узора или даже целой композиции превращает даже простой рисунок в произведение искусства за счет объема изображения. Непередаваемо играет с Led-подсветкой в качестве элемента декора, а метод золочения придает работе с объемной гравировкой поистине царский вид.</p> </cut>', 'volume.jpg');
INSERT INTO categories VALUES (5, 'Зеркальные панно', '<p>Декоративные настенные и потолочные композиции. На кухне, в прихожей, холле или столовой-гостиной – это один из наиболее доступных и неповторимых материалов для декора интерьера, а многообразие геометрических форм и вариантов дополнительной обработки (старение, золочение, дополнительное амальгирование узора) позволяют зеркальному панно приукрасить любой интерьер. </p> </cut>', 'panno.jpg');
INSERT INTO categories VALUES (6, 'Венецианские зеркала', '<p>Особенность венецианских зеркал заключается в их оправе. Изначально обрамлению придавалось особое значение. Еще в 16-17 веках рама покрывалась стеклом, отполированным до блеска, украшалась стеклянными листьями и цветами. Далее стекло раскрашивали масляными красками, а деревянные части рамы покрывали позолотой или лаком в соответствии со стилизацией и вкусами того времени. </p> </cut>
<p>Несмотря на появление различных технологий, и современных методов изготовления качественных зеркал, венецианские изделия отличаются от «ширпотреба» индивидуальным подходом. Каждое изделие изготавливается только вручную, поэтому ценность таких изделий достаточно высока.
Несмотря на появление на рынке зеркал изделий от различных производителей, в наше время венецианские зеркала по-прежнему считаются символом роскоши и изящества. В интерьировании они используются как самостоя
тельные элементы декора, либо являются частью общей композиции. Особый шарм такому изделию придают вручную изготовленный фацет на небольших криволинейных фрагментах зеркал и гармонично вписанный орнамент, который может быть смешением самых различных стилей и техник. </p>
<p>Сложные композиции из зеркал относятся также к категории зеркал ручной работы. Они выполняются на многоуровневой подложке из МДФ, дополняются различными декоративными элементами. Разработка зеркальных композиций и их сборка – это, поистине, верх мастерства стекольщика, ведь каждый отдельный элемент выполняется вручную, после чего компонуется в единый проект. В этот же момент выбирается цвет изделия, а это может быть традиционное серебро или серебро+бронза, но может выглядеть и как состаренное зеркало. Само изделие, как и требует того технология, изготавливается только вручную, На готовые зеркальные поверхности наносится декоративный рисунок, либо зеркало искусственно состаривается. После чего, производится сборка всего изделия на жесткую подложку.</p>
', 'venice.jpg');
INSERT INTO categories VALUES (7, 'Зеркала с золочением', '<p>Одним из великолепных вариантом украшения вашего интерьера является сусальное золочение и серебрение изделий. Роскошь королевских дворцовых залов и комнат безусловно обусловлена наличием позолоты в их внутреннем и внешнем убранстве.</p></cut>', 'golden.jpg');
INSERT INTO categories VALUES (8, 'Зеркала-привидения', '<p>Красивые зеркала-призраки могут стать изюминкой вашего интерьера.</p></cut>', 'ghost.jpg');


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categories_id_seq', 3, true);


--
-- Data for Name: mirrors; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO mirrors VALUES (2, 'Зеркало Шебби-Шик', 'Состаренное зеркало выполненное в стиле Шебби-шик, обрамлено в деревянный багет с кракелюром и украшено лепниной.', '{"photos":["g1.jpg","g2.jpg","g3.jpg"]}', 40000);
INSERT INTO mirrors VALUES (1, 'Сатарье', 'Это зеркало прекрасно впишется в ваш интерьер.', '{"photos":["sat1.jpg","sat2.jpg","sat3.jpg","sat4.jpg","sat5.jpg","sat6.jpg"]}', 50000);


--
-- Name: mirrors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('mirrors_id_seq', 1, false);


--
-- Data for Name: opinions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO opinions VALUES ('Марина А.', 'otzyv-9.jpg', 'Благодарю зеркальную мастерскую Vintage-Design за прекрасную идею и великолепное её воплощение! Обычная прихожая с винтажным зеркалом в раме с позолотой превращается в приёмную вельможи. Обязательно сделаю заказ для подарка, другого такого нигде не найти. Еще раз огромное спасибо!', 1);
INSERT INTO opinions VALUES ('Дизайнер Андрюшов Михаил.', 'otzyv-1.png', 'Доброго времени суток! За долгое время проектно-дизайнерской деятельности я наконец-то нашел в лице зеркальной мастерской Vintage-Design надежного партнера, способного воплотить в жизнь любой, даже самый креативный и сложный замысел.', 2);
INSERT INTO opinions VALUES ('Александр Корнеев', 'otzyv-3.jpg', 'Спасибо винтажной мастерской за отличное зеркало! Заказал в офис, очень доволен.', 3);
INSERT INTO opinions VALUES ('Рассказова Жанна', 'otzyv-4.jpg', 'Всю жизнь мечтала о красивом зеркале как у принцессы. В компании
                    Винтаж Дизайн осуществили мою мечту и создали чудесное зеркало с состаренном стиле!
                    Обязательно посоветую именно вашу мастерскую своим друзьям и знакомым!', 4);
INSERT INTO opinions VALUES ('Елистратов Евгений', 'otzyv-5.jpg', 'Благодарю зеркальную мастерскую Vintage Design за чудесное
                    исполнение моего заказа. Радуюсь отличному зеркалу с Led-подсветкой. Спасибо, друзья!', 5);
INSERT INTO opinions VALUES ('Маркелова Анна', 'otzyv-6.jpg', 'Хотелось бы поблагодарить коллектив Vintage-Design за
                    целеустремленность, ответственность и превосходное качество работы, которое не
                    вызывает нареканий даже у самого требовательного и придирчивого заказчика. Рада
                    сотрудничеству и желаю дальнейшего творческого роста и успеха.', 6);
INSERT INTO opinions VALUES ('Федосеева Елена', 'otzyv-7.jpg', 'Спасибо ребятам из мастерской Винтаж Дизайн за качественную и
                    красивую работу. Сделала подарок любимой маме, преобразовав ее старое зеркало. Вы
                    профессионалы с большой буквы!', 7);
INSERT INTO opinions VALUES ('Собриятнова Валентина', 'otzyv-8.jpg', 'Я парикмахер. И для моей парикмахерской очень важно показать клиенту, как он выглядит. Клиенты всегда обращают внимание на чудесную работу мастерской Vintage Design! Довольные клиенты - огромный плюс для любого бизнеса.', 8);
INSERT INTO opinions VALUES ('Поротникова Л.', 'otzyv-2.png', 'Спасибо мастерской за 2 прекрасно исполненных зеркала, оба отлино вписались в интерьер флористиеской студии.Задача была поставлена, перед мастерами, твореская и ими были найдены лучшие эстетические решения. Сотруднечество доставило радость. Еще не раз обращусь за помощью.', 9);


--
-- Name: opinions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('opinions_id_seq', 9, true);


--
-- Data for Name: orderMirrors; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "orderMirrors" VALUES (1, '1', 'v1.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (2, '2', 'v2.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (3, '3', 'v3.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (4, '4', 'v4.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (5, '5', 'v5.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (6, '6', 'v6.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (7, '7', 'v7.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (8, '8', 'v8.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (9, '9', 'v9.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (10, '10', 'v10.jpg', 'Венецианские зеркала');
INSERT INTO "orderMirrors" VALUES (11, '1', 'e1.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (12, '2', 'e2.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (13, '3', 'e3.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (14, '4', 'e4.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (15, '5', 'e5.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (16, '1', 'g1.jpg', 'Зеркала с золочением');
INSERT INTO "orderMirrors" VALUES (17, '2', 'g2.jpg', 'Зеркала с золочением');
INSERT INTO "orderMirrors" VALUES (18, '3', 'g3.jpg', 'Зеркала с золочением');
INSERT INTO "orderMirrors" VALUES (19, '4', 'g4.jpg', 'Зеркала с золочением');
INSERT INTO "orderMirrors" VALUES (20, '5', 'g5.jpg', 'Зеркала с золочением');
INSERT INTO "orderMirrors" VALUES (21, '1', 'vo1.jpg', 'Объемная гравировка');
INSERT INTO "orderMirrors" VALUES (22, '2', 'vo2.jpg', 'Объемная гравировка');
INSERT INTO "orderMirrors" VALUES (23, '3', 'vo3.jpg', 'Объемная гравировка');
INSERT INTO "orderMirrors" VALUES (24, '4', 'vo4.jpg', 'Объемная гравировка');
INSERT INTO "orderMirrors" VALUES (25, '5', 'vo5.jpg', 'Объемная гравировка');
INSERT INTO "orderMirrors" VALUES (26, '6', 'vo6.jpg', 'Объемная гравировка');
INSERT INTO "orderMirrors" VALUES (27, '1', 'el1.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (28, '2', 'el2.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (29, '3', 'el3.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (30, '4', 'el4.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (31, '5', 'el5.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (32, '6', 'el6.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (33, '1', 'p1.jpg', 'Зеркальные панно');
INSERT INTO "orderMirrors" VALUES (34, '2', 'p2.jpg', 'Зеркальные панно');
INSERT INTO "orderMirrors" VALUES (35, '3', 'p3.jpg', 'Зеркальные панно');
INSERT INTO "orderMirrors" VALUES (36, '1', 'i1.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (37, '2', 'i2.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (38, '3', 'i3.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (39, '4', 'i4.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (40, '5', 'i5.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (41, '6', 'i6.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (42, '7', 'i7.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (43, '6', 'e6.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (44, '8', 'i8.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (45, '9', 'i9.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (46, '10', 'i10.jpg', 'Интерьерные зеркала');
INSERT INTO "orderMirrors" VALUES (47, '7', 'el7.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (48, '8', 'el8.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (49, '7', 'e7.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (50, '8', 'e8.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (51, '9', 'e9.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (52, '10', 'e10.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (53, '11', 'e11.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (54, '12', 'e12.jpg', 'Эгломизе');
INSERT INTO "orderMirrors" VALUES (55, '9', 'e9.jpg', 'Состаренное зеркало');
INSERT INTO "orderMirrors" VALUES (56, '1', 'gh1.jpg', 'Зеркала-привидения');
INSERT INTO "orderMirrors" VALUES (57, '2', 'gh2.jpg', 'Зеркала-привидения');
INSERT INTO "orderMirrors" VALUES (58, '3', 'gh3.jpg', 'Зеркала-привидения');
INSERT INTO "orderMirrors" VALUES (59, '4', 'gh4.jpg', 'Зеркала-привидения');
INSERT INTO "orderMirrors" VALUES (60, '5', 'gh5.jpg', 'Зеркала-привидения');
INSERT INTO "orderMirrors" VALUES (61, '6', 'gh6.jpg', 'Зеркала-привидения');


--
-- Name: orderMirrors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"orderMirrors_id_seq"', 1, false);


--
-- Data for Name: play_evolutions; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: articles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: mirrors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mirrors
    ADD CONSTRAINT mirrors_pkey PRIMARY KEY (id);


--
-- Name: opinions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY opinions
    ADD CONSTRAINT opinions_pkey PRIMARY KEY (id);


--
-- Name: orderMirrors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "orderMirrors"
    ADD CONSTRAINT "orderMirrors_pkey" PRIMARY KEY (id, category);


--
-- Name: play_evolutions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY play_evolutions
    ADD CONSTRAINT play_evolutions_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

