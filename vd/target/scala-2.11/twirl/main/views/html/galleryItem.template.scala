
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object galleryItem extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Category,Seq[String],Seq[OrderMirror],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(category: Category, descriptions:Seq[String], orderMirrors:Seq[OrderMirror]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.79*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),_display_(/*4.2*/header("")),format.raw/*4.12*/("""
"""),format.raw/*5.1*/("""<article>
    <div class="article-border">
        <div class="container">
            <div class="grid-12">
                <div class="gallery">
                    <div class="gallery-text">
                        <h2>"""),_display_(/*11.30*/category/*11.38*/.name),format.raw/*11.43*/("""</h2>
                        """),_display_(/*12.26*/Option(descriptions)/*12.46*/.map/*12.50*/ { paragraphs =>_display_(Seq[Any](format.raw/*12.66*/("""
                        """),_display_(/*13.26*/paragraphs/*13.36*/.map/*13.40*/ { paragraph =>_display_(Seq[Any](format.raw/*13.55*/("""
                            """),format.raw/*14.29*/("""<p>"""),_display_(/*14.33*/paragraph),format.raw/*14.42*/("""</p>
                        """)))}),format.raw/*15.26*/("""
                        """)))}),format.raw/*16.26*/("""

                    """),format.raw/*18.21*/("""</div>
                    <div class="col5">
                        """),_display_(/*20.26*/for(orderMirror <- orderMirrors) yield /*20.58*/ {_display_(Seq[Any](format.raw/*20.60*/("""
                        """),format.raw/*21.25*/("""<div class="item">
                            <div class="gallery-img cl-effect-10">
                                <a href=""""),_display_(/*23.43*/routes/*23.49*/.Assets.versioned("images/orderMirror/" + orderMirror.photo)),format.raw/*23.109*/("""" data-lightbox="roadtrip" data-hover=""""),_display_(/*23.149*/orderMirror/*23.160*/.name),format.raw/*23.165*/("""">
                                    <img src=""""),_display_(/*24.48*/routes/*24.54*/.Assets.versioned("images/orderMirror/" + orderMirror.photo)),format.raw/*24.114*/("""">
                                    <h3>"""),_display_(/*25.42*/orderMirror/*25.53*/.name),format.raw/*25.58*/("""</h3>
                                </a>
                            </div>
                        </div>
                        """)))}),format.raw/*29.26*/("""
                    """),format.raw/*30.21*/("""</div>
                </div>
            </div>
        </div>
    </div>
</article>
""")))}))}
  }

  def render(category:Category,descriptions:Seq[String],orderMirrors:Seq[OrderMirror]): play.twirl.api.HtmlFormat.Appendable = apply(category,descriptions,orderMirrors)

  def f:((Category,Seq[String],Seq[OrderMirror]) => play.twirl.api.HtmlFormat.Appendable) = (category,descriptions,orderMirrors) => apply(category,descriptions,orderMirrors)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/galleryItem.scala.html
                  HASH: aacca9a88b0bdfadd428ad3ee8743ee88b5bd341
                  MATRIX: 542->1|707->78|735->81|746->85|784->87|811->89|841->99|868->100|1118->323|1135->331|1161->336|1219->367|1248->387|1261->391|1315->407|1368->433|1387->443|1400->447|1453->462|1510->491|1541->495|1571->504|1632->534|1689->560|1739->582|1837->653|1885->685|1925->687|1978->712|2133->840|2148->846|2230->906|2298->946|2319->957|2346->962|2423->1012|2438->1018|2520->1078|2591->1122|2611->1133|2637->1138|2802->1272|2851->1293
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|26->5|32->11|32->11|32->11|33->12|33->12|33->12|33->12|34->13|34->13|34->13|34->13|35->14|35->14|35->14|36->15|37->16|39->18|41->20|41->20|41->20|42->21|44->23|44->23|44->23|44->23|44->23|44->23|45->24|45->24|45->24|46->25|46->25|46->25|50->29|51->30
                  -- GENERATED --
              */
          