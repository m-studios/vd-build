
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object about extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](_display_(/*1.2*/main/*1.6*/ {_display_(Seq[Any](format.raw/*1.8*/("""
"""),_display_(/*2.2*/header("about")),format.raw/*2.17*/("""

"""),format.raw/*4.1*/("""<article>
   <div class="article-border">
      <div class="container">
         <div class="grid-12">
            <div class="article-text">
               <p>Основой деятельности мастерской <span>Vintage Design</span> является преображение зеркала или стекла до состояния произведения искусства. Мы искренне рады смелым идеям и готовы воплотить их в жизнь. Приоритетом для себя мы выбрали создание единичных и неповторимых работ из зеркал и создание индивидуальных работ для интерьера. Мы находимся в постоянном движении и поиске новых решений и возможностей.</p>
               <p class="article-color">Там где другие разводят руками и говорят что это невозможно, мы добиваемся цели.</p>
               <p>Мастерская <span>Vintage Design</span> предлагает возможность соединения множества техник в одном полотне. Плодами наших исследований стало возрождение техники <b>Эгломизе</b>, стекло с нанесением зеркального рисунка, зеркала призраки и наша гордость <b>Криптотипия</b>.</p>
               <div class="border-article"></div>
               <p>Мы не остановимся на достигнутом и будем развивать направления дальше. Мы дорожим своим именем и отвечаем за качество наших работ. Работа с заказчиком ведется индивидуально с учетом его потребностей. Цена на каждую работу считается отдельно исходя от размера, типа и сложности.</p>
            </div>
         </div>
      </div>
   </div>
</article>
""")))}))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/about.scala.html
                  HASH: 82159f48aa9ab37297ed2ec88755ca006155f3dd
                  MATRIX: 580->1|591->5|629->7|656->9|691->24|719->26
                  LINES: 22->1|22->1|22->1|23->2|23->2|25->4
                  -- GENERATED --
              */
          