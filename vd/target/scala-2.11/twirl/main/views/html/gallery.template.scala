
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object gallery extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Seq[Category],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(categorySeq: Seq[Category]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.30*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),_display_(/*4.2*/header("gallery")),format.raw/*4.19*/("""
"""),format.raw/*5.1*/("""<article>
    <div class="article-border">
        <div class="container">
            <div class="grid-12">
                <div class="gallery">
                    <div class="col3">

                        """),_display_(/*12.26*/Option(categorySeq)/*12.45*/.map/*12.49*/ { categories =>_display_(Seq[Any](format.raw/*12.65*/("""
                        """),_display_(/*13.26*/categories/*13.36*/.map/*13.40*/ { category =>_display_(Seq[Any](format.raw/*13.54*/("""
                        """),format.raw/*14.25*/("""<div class="item">
                            <div class="gallery-img cl-effect-10">
                                <a href=""""),_display_(/*16.43*/routes/*16.49*/.VintageDesign.galleryItem(category.name)),format.raw/*16.90*/("""" data-hover=""""),_display_(/*16.105*/category/*16.113*/.name),format.raw/*16.118*/("""">
                                    <img src=""""),_display_(/*17.48*/routes/*17.54*/.Assets.versioned("images/category/" + category.photo)),format.raw/*17.108*/("""">
                                    <h3>"""),_display_(/*18.42*/category/*18.50*/.name),format.raw/*18.55*/("""</h3>
                                </a>
                            </div>
                        </div>
                        """)))}),format.raw/*22.26*/("""
                        """)))}),format.raw/*23.26*/("""
                    """),format.raw/*24.21*/("""</div>
                </div>
            </div>
        </div>
    </div>
</article>
""")))}))}
  }

  def render(categorySeq:Seq[Category]): play.twirl.api.HtmlFormat.Appendable = apply(categorySeq)

  def f:((Seq[Category]) => play.twirl.api.HtmlFormat.Appendable) = (categorySeq) => apply(categorySeq)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/gallery.scala.html
                  HASH: 3fd9f498472889065d8ce9e6f6e87f52d67b25e7
                  MATRIX: 514->1|630->29|658->32|669->36|707->38|734->40|771->57|798->58|1037->270|1065->289|1078->293|1132->309|1185->335|1204->345|1217->349|1269->363|1322->388|1477->516|1492->522|1554->563|1597->578|1615->586|1642->591|1719->641|1734->647|1810->701|1881->745|1898->753|1924->758|2089->892|2146->918|2195->939
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|26->5|33->12|33->12|33->12|33->12|34->13|34->13|34->13|34->13|35->14|37->16|37->16|37->16|37->16|37->16|37->16|38->17|38->17|38->17|39->18|39->18|39->18|43->22|44->23|45->24
                  -- GENERATED --
              */
          