
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object order extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[Order],Boolean,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(form:Form[Order], messageSent:Boolean = false):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.49*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),_display_(/*4.2*/header("")),format.raw/*4.12*/("""
 """),format.raw/*5.2*/("""<article>
     """),_display_(/*6.7*/orderForm("Cделать заказ", form, messageSent)),format.raw/*6.52*/("""
 """),format.raw/*7.2*/("""</article>
     """),_display_(/*8.7*/mapAndContacts()),format.raw/*8.23*/("""

""")))}))}
  }

  def render(form:Form[Order],messageSent:Boolean): play.twirl.api.HtmlFormat.Appendable = apply(form,messageSent)

  def f:((Form[Order],Boolean) => play.twirl.api.HtmlFormat.Appendable) = (form,messageSent) => apply(form,messageSent)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/order.scala.html
                  HASH: db89aa97b528b9d8d8c3f4b37ea53619501d81c3
                  MATRIX: 518->1|653->48|681->51|692->55|730->57|757->59|787->69|815->71|856->87|921->132|949->134|991->151|1027->167
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|26->5|27->6|27->6|28->7|29->8|29->8
                  -- GENERATED --
              */
          