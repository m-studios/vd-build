
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object addOpinion extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[Opinion],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(opinionForm:Form[Opinion]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import views.form.AdminAddItemFieldConstructorHelper._

Seq[Any](format.raw/*1.29*/("""

"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/adminMain/*5.11*/ {_display_(Seq[Any](format.raw/*5.13*/("""
"""),_display_(/*6.2*/helper/*6.8*/.form(action = routes.AdminVintageDesign.postOpinion, 'enctype -> "multipart/form-data")/*6.96*/ {_display_(Seq[Any](format.raw/*6.98*/("""
"""),_display_(/*7.2*/helper/*7.8*/.inputText(opinionForm("name"), 'placeholder -> "Имя")),format.raw/*7.62*/("""
"""),_display_(/*8.2*/helper/*8.8*/.inputText(opinionForm("text"), 'cols -> "80", 'rows -> "11")/*8.69*/(handler = textAreaConstructor, implicitly[Lang])),format.raw/*8.118*/("""
"""),format.raw/*9.1*/("""<input type="file" name="photo" required>
<p><input type="submit" value="Отправить"></p>
""")))}),format.raw/*11.2*/("""

"""),format.raw/*13.1*/("""<script>initSample();</script>
<script type="text/javascript" src=""""),_display_(/*14.38*/routes/*14.44*/.Assets.versioned("javascripts/dropzoneInitializer.js")),format.raw/*14.99*/(""""></script>
""")))}),format.raw/*15.2*/("""
"""))}
  }

  def render(opinionForm:Form[Opinion]): play.twirl.api.HtmlFormat.Appendable = apply(opinionForm)

  def f:((Form[Opinion]) => play.twirl.api.HtmlFormat.Appendable) = (opinionForm) => apply(opinionForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/addOpinion.scala.html
                  HASH: dd81578cee6333fbfea5bc3c76b636f5a25c0ff4
                  MATRIX: 517->1|686->28|714->86|741->88|758->97|797->99|824->101|837->107|933->195|972->197|999->199|1012->205|1086->259|1113->261|1126->267|1195->328|1265->377|1292->378|1412->468|1441->470|1536->538|1551->544|1627->599|1670->612
                  LINES: 19->1|22->1|24->4|25->5|25->5|25->5|26->6|26->6|26->6|26->6|27->7|27->7|27->7|28->8|28->8|28->8|28->8|29->9|31->11|33->13|34->14|34->14|34->14|35->15
                  -- GENERATED --
              */
          