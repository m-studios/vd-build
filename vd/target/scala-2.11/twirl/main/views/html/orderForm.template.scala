
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object orderForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,Form[Order],Boolean,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title:String, orderForm:Form[Order], messageSent:Boolean = false):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import views.form.OrderFieldConstructorHelper._

Seq[Any](format.raw/*1.68*/("""

"""),format.raw/*4.1*/("""
"""),format.raw/*5.1*/("""<div class="form-bg-color">
    <div class="container">
        <div class="grid-12">
            <h2>"""),_display_(/*8.18*/title),format.raw/*8.23*/("""</h2>
            """),_display_(/*9.14*/helper/*9.20*/.form(action = routes.VintageDesign.orderPost)/*9.66*/ {_display_(Seq[Any](format.raw/*9.68*/("""
            """),format.raw/*10.13*/("""<div class="col2">
                <div class="item">
                    <p class="p-form">Ваше имя:* </p>
                    """),_display_(/*13.22*/helper/*13.28*/.inputText(orderForm("name"))/*13.57*/(handler = requiredField, implicitly[Lang])),format.raw/*13.100*/("""
                    """),format.raw/*14.21*/("""<p class="p-form">Ваш телефон:* </p>
                    """),_display_(/*15.22*/helper/*15.28*/.inputText(orderForm("phone"))/*15.58*/(handler = requiredField, implicitly[Lang])),format.raw/*15.101*/("""
                    """),format.raw/*16.21*/("""<p class="p-form">Ваш e-mail: </p>
                    """),_display_(/*17.22*/helper/*17.28*/.inputText(orderForm("email"))),format.raw/*17.58*/("""
                """),format.raw/*18.17*/("""</div>
                <div class="item">
                    <p class="p-form">Поле для текста:* </p>
                    """),_display_(/*21.22*/helper/*21.28*/.inputText(orderForm("text"), 'cols -> "80", 'rows -> "11")/*21.87*/(handler = textAreaConstructor, implicitly[Lang])),format.raw/*21.136*/("""
                """),format.raw/*22.17*/("""</div>
            </div>
            <p class="form-info">* - поля обязательные для заполнения</p>
            """),_display_(/*25.14*/if(messageSent)/*25.29*/ {_display_(Seq[Any](format.raw/*25.31*/("""
                """),format.raw/*26.17*/("""<p class="success">Ваше сообщение отправлено</p>
            """)))}/*27.15*/else/*27.20*/{_display_(Seq[Any](format.raw/*27.21*/("""
                """),format.raw/*28.17*/("""<p><input class="bt" type="submit" value="Отправить"></p>
            """)))}),format.raw/*29.14*/("""
            """)))}),format.raw/*30.14*/("""
        """),format.raw/*31.9*/("""</div>
    </div>
</div>"""))}
  }

  def render(title:String,orderForm:Form[Order],messageSent:Boolean): play.twirl.api.HtmlFormat.Appendable = apply(title,orderForm,messageSent)

  def f:((String,Form[Order],Boolean) => play.twirl.api.HtmlFormat.Appendable) = (title,orderForm,messageSent) => apply(title,orderForm,messageSent)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/orderForm.scala.html
                  HASH: a1e698a265d1c7825b025eaef8bbf0fcf91fbb89
                  MATRIX: 529->1|730->67|758->118|785->119|914->222|939->227|984->246|998->252|1052->298|1091->300|1132->313|1288->442|1303->448|1341->477|1406->520|1455->541|1540->599|1555->605|1594->635|1659->678|1708->699|1791->755|1806->761|1857->791|1902->808|2053->932|2068->938|2136->997|2207->1046|2252->1063|2392->1176|2416->1191|2456->1193|2501->1210|2582->1273|2595->1278|2634->1279|2679->1296|2781->1367|2826->1381|2862->1390
                  LINES: 19->1|22->1|24->4|25->5|28->8|28->8|29->9|29->9|29->9|29->9|30->10|33->13|33->13|33->13|33->13|34->14|35->15|35->15|35->15|35->15|36->16|37->17|37->17|37->17|38->18|41->21|41->21|41->21|41->21|42->22|45->25|45->25|45->25|46->26|47->27|47->27|47->27|48->28|49->29|50->30|51->31
                  -- GENERATED --
              */
          