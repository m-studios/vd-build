
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object article extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Article,Seq[String],Seq[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(article:Article, descriptions:Seq[String], photos:Seq[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.65*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),_display_(/*4.2*/header("")),format.raw/*4.12*/("""
"""),format.raw/*5.1*/("""<article>
    <div class="article-border">
        <div class="container">
            <div class="grid-12">
                <div class="gallery">
                    <div class="gallery-text">
                        <h2>"""),_display_(/*11.30*/article/*11.37*/.name),format.raw/*11.42*/("""</h2>
                        """),_display_(/*12.26*/Option(descriptions)/*12.46*/.map/*12.50*/ { paragraphs =>_display_(Seq[Any](format.raw/*12.66*/("""
                        """),_display_(/*13.26*/paragraphs/*13.36*/.map/*13.40*/ { paragraph =>_display_(Seq[Any](format.raw/*13.55*/("""
                        """),format.raw/*14.25*/("""<p>"""),_display_(/*14.29*/paragraph),format.raw/*14.38*/("""</p>
                        """)))}),format.raw/*15.26*/("""
                        """)))}),format.raw/*16.26*/("""
                    """),format.raw/*17.21*/("""</div>

                    <div class="col5">
                        """),_display_(/*20.26*/for( i <- 0 to photos.length-1) yield /*20.57*/ {_display_(Seq[Any](format.raw/*20.59*/("""
                            """),_display_(/*21.30*/defining(photos(i))/*21.49*/ { photo =>_display_(Seq[Any](format.raw/*21.60*/("""
                            """),_display_(/*22.30*/defining(i+1)/*22.43*/ { index =>_display_(Seq[Any](format.raw/*22.54*/("""
                        """),format.raw/*23.25*/("""<div class="item">
                            <div class="gallery-img cl-effect-10">
                                <a href=""""),_display_(/*25.43*/routes/*25.49*/.Assets.versioned("images/posts/" + photo)),format.raw/*25.91*/("""" data-lightbox="roadtrip" data-hover=""""),_display_(/*25.131*/index/*25.136*/.toString),format.raw/*25.145*/("""">
                                <img src=""""),_display_(/*26.44*/routes/*26.50*/.Assets.versioned("images/posts/" + photo)),format.raw/*26.92*/("""">
                                <h3>"""),_display_(/*27.38*/index/*27.43*/.toString),format.raw/*27.52*/("""</h3>
                                </a>
                            </div>
                        </div>
                        """)))}),format.raw/*31.26*/("""
                            """)))}),format.raw/*32.30*/("""
                        """)))}),format.raw/*33.26*/("""
                    """),format.raw/*34.21*/("""</div>
                </div>
            </div>
        </div>
    </div>
</article>
""")))}))}
  }

  def render(article:Article,descriptions:Seq[String],photos:Seq[String]): play.twirl.api.HtmlFormat.Appendable = apply(article,descriptions,photos)

  def f:((Article,Seq[String],Seq[String]) => play.twirl.api.HtmlFormat.Appendable) = (article,descriptions,photos) => apply(article,descriptions,photos)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/article.scala.html
                  HASH: 080e6369e9923747ce5ed6b1f96b0b9ea953e61c
                  MATRIX: 532->1|683->64|711->67|722->71|760->73|787->75|817->85|844->86|1094->309|1110->316|1136->321|1194->352|1223->372|1236->376|1290->392|1343->418|1362->428|1375->432|1428->447|1481->472|1512->476|1542->485|1603->515|1660->541|1709->562|1808->634|1855->665|1895->667|1952->697|1980->716|2029->727|2086->757|2108->770|2157->781|2210->806|2365->934|2380->940|2443->982|2511->1022|2526->1027|2557->1036|2630->1082|2645->1088|2708->1130|2775->1170|2789->1175|2819->1184|2984->1318|3045->1348|3102->1374|3151->1395
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|26->5|32->11|32->11|32->11|33->12|33->12|33->12|33->12|34->13|34->13|34->13|34->13|35->14|35->14|35->14|36->15|37->16|38->17|41->20|41->20|41->20|42->21|42->21|42->21|43->22|43->22|43->22|44->23|46->25|46->25|46->25|46->25|46->25|46->25|47->26|47->26|47->26|48->27|48->27|48->27|52->31|53->32|54->33|55->34
                  -- GENERATED --
              */
          