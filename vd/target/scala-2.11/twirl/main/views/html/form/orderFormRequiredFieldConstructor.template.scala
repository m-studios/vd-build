
package views.html.form

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object orderFormRequiredFieldConstructor extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[helper.FieldElements,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(elements: helper.FieldElements):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<div class=""""),_display_(/*3.14*/if(elements.hasErrors)/*3.36*/ {_display_(Seq[Any](format.raw/*3.38*/("""error""")))}),format.raw/*3.44*/("""">
    """),_display_(/*4.6*/defining(elements.field.value)/*4.36*/ { value =>_display_(Seq[Any](format.raw/*4.47*/("""
    """),format.raw/*5.5*/("""<input class="form-input" type="text" value=""""),_display_(/*5.51*/if(value != None)/*5.68*/{_display_(_display_(/*5.70*/value/*5.75*/.get))}),format.raw/*5.80*/("""" name=""""),_display_(/*5.89*/elements/*5.97*/.id),format.raw/*5.100*/("""" size="40" required>
    """)))}),format.raw/*6.6*/("""
"""),format.raw/*7.1*/("""</div>"""))}
  }

  def render(elements:helper.FieldElements): play.twirl.api.HtmlFormat.Appendable = apply(elements)

  def f:((helper.FieldElements) => play.twirl.api.HtmlFormat.Appendable) = (elements) => apply(elements)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/form/orderFormRequiredFieldConstructor.scala.html
                  HASH: a0b8df47904de322ec8972e6a5f51ad4efd53634
                  MATRIX: 552->1|672->33|700->35|739->48|769->70|808->72|844->78|877->86|915->116|963->127|994->132|1066->178|1091->195|1120->197|1133->202|1160->207|1195->216|1211->224|1235->227|1291->254|1318->255
                  LINES: 19->1|22->1|24->3|24->3|24->3|24->3|24->3|25->4|25->4|25->4|26->5|26->5|26->5|26->5|26->5|26->5|26->5|26->5|26->5|27->6|28->7
                  -- GENERATED --
              */
          