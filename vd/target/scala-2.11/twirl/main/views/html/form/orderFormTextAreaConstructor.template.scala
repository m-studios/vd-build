
package views.html.form

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object orderFormTextAreaConstructor extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[helper.FieldElements,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(elements: helper.FieldElements):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<div class=""""),_display_(/*3.14*/if(elements.hasErrors)/*3.36*/ {_display_(Seq[Any](format.raw/*3.38*/("""error""")))}),format.raw/*3.44*/("""">
    """),_display_(/*4.6*/defining(elements.field.value)/*4.36*/ { value =>_display_(Seq[Any](format.raw/*4.47*/("""
    """),format.raw/*5.5*/("""<textarea class="form-input" name=""""),_display_(/*5.41*/elements/*5.49*/.id),format.raw/*5.52*/("""" cols="80" rows="11" required>"""),_display_(/*5.84*/if(value != None)/*5.101*/{_display_(_display_(/*5.103*/value/*5.108*/.get))}),format.raw/*5.113*/("""</textarea>
    """)))}),format.raw/*6.6*/("""
"""),format.raw/*7.1*/("""</div>"""))}
  }

  def render(elements:helper.FieldElements): play.twirl.api.HtmlFormat.Appendable = apply(elements)

  def f:((helper.FieldElements) => play.twirl.api.HtmlFormat.Appendable) = (elements) => apply(elements)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/form/orderFormTextAreaConstructor.scala.html
                  HASH: 24ece64c65f74bda083c7e1eb39dcc0490223a3d
                  MATRIX: 547->1|667->33|695->35|734->48|764->70|803->72|839->78|872->86|910->116|958->127|989->132|1051->168|1067->176|1090->179|1148->211|1174->228|1204->230|1218->235|1246->240|1292->257|1319->258
                  LINES: 19->1|22->1|24->3|24->3|24->3|24->3|24->3|25->4|25->4|25->4|26->5|26->5|26->5|26->5|26->5|26->5|26->5|26->5|26->5|27->6|28->7
                  -- GENERATED --
              */
          