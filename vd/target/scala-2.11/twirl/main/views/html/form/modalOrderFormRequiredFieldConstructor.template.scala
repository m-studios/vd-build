
package views.html.form

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object modalOrderFormRequiredFieldConstructor extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[helper.FieldElements,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(elements: helper.FieldElements):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<div class=""""),_display_(/*3.14*/if(elements.hasErrors)/*3.36*/ {_display_(Seq[Any](format.raw/*3.38*/("""error""")))}),format.raw/*3.44*/("""">
    <input class="form-input" type="text" value="" name=""""),_display_(/*4.59*/elements/*4.67*/.id),format.raw/*4.70*/("""" size="40" required>
</div>"""))}
  }

  def render(elements:helper.FieldElements): play.twirl.api.HtmlFormat.Appendable = apply(elements)

  def f:((helper.FieldElements) => play.twirl.api.HtmlFormat.Appendable) = (elements) => apply(elements)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/form/modalOrderFormRequiredFieldConstructor.scala.html
                  HASH: 9f60d7b28750886bce5231eecc2769a72f5f0277
                  MATRIX: 557->1|677->33|705->35|744->48|774->70|813->72|849->78|936->139|952->147|975->150
                  LINES: 19->1|22->1|24->3|24->3|24->3|24->3|24->3|25->4|25->4|25->4
                  -- GENERATED --
              */
          