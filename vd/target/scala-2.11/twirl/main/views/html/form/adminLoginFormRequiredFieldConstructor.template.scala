
package views.html.form

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object adminLoginFormRequiredFieldConstructor extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[helper.FieldElements,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(elements: helper.FieldElements):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<input class="field" type=""""),_display_(/*3.29*/if(elements.id.equals("password"))/*3.63*/ {_display_(Seq[Any](format.raw/*3.65*/("""password""")))}/*3.75*/else/*3.80*/{_display_(Seq[Any](format.raw/*3.81*/("""text""")))}),format.raw/*3.86*/("""" name=""""),_display_(/*3.95*/elements/*3.103*/.id),format.raw/*3.106*/("""" size="40" """),_display_(/*3.119*/toHtmlArgs(elements.args)),format.raw/*3.144*/(""" """),format.raw/*3.145*/("""required>"""))}
  }

  def render(elements:helper.FieldElements): play.twirl.api.HtmlFormat.Appendable = apply(elements)

  def f:((helper.FieldElements) => play.twirl.api.HtmlFormat.Appendable) = (elements) => apply(elements)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/form/adminLoginFormRequiredFieldConstructor.scala.html
                  HASH: 2f5ed11896a50bb0962c652efa0ad2c3187f78c4
                  MATRIX: 557->1|677->33|705->35|759->63|801->97|840->99|867->109|879->114|917->115|952->120|987->129|1004->137|1028->140|1068->153|1114->178|1143->179
                  LINES: 19->1|22->1|24->3|24->3|24->3|24->3|24->3|24->3|24->3|24->3|24->3|24->3|24->3|24->3|24->3|24->3
                  -- GENERATED --
              */
          