
package views.html.form

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object adminAddItemFormRequiredFieldConstructor extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[helper.FieldElements,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(elements: helper.FieldElements):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<div>
    <input class="field" name=""""),_display_(/*4.33*/elements/*4.41*/.id),format.raw/*4.44*/("""" size="40" """),_display_(/*4.57*/toHtmlArgs(elements.args)),format.raw/*4.82*/(""" """),format.raw/*4.83*/("""style="margin: 10px 10px;" required>
</div>
"""))}
  }

  def render(elements:helper.FieldElements): play.twirl.api.HtmlFormat.Appendable = apply(elements)

  def f:((helper.FieldElements) => play.twirl.api.HtmlFormat.Appendable) = (elements) => apply(elements)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/form/adminAddItemFormRequiredFieldConstructor.scala.html
                  HASH: 77c1c63de5953aeb585e53a5feffc3fa43bca25b
                  MATRIX: 559->1|679->33|707->35|771->73|787->81|810->84|849->97|894->122|922->123
                  LINES: 19->1|22->1|24->3|25->4|25->4|25->4|25->4|25->4|25->4
                  -- GENERATED --
              */
          