
package views.html.form

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object orderFormFieldConstructor extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[helper.FieldElements,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(elements: helper.FieldElements):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.34*/("""

"""),format.raw/*3.1*/("""<div class=""""),_display_(/*3.14*/if(elements.hasErrors)/*3.36*/ {_display_(Seq[Any](format.raw/*3.38*/("""error""")))}),format.raw/*3.44*/("""">
    """),_display_(/*4.6*/defining(elements.field.value)/*4.36*/ { value =>_display_(Seq[Any](format.raw/*4.47*/("""
    """),format.raw/*5.5*/("""<input class="form-input" type="text" value=""""),_display_(/*5.51*/if(value != None)/*5.68*/{_display_(_display_(/*5.70*/value/*5.75*/.get))}),format.raw/*5.80*/("""" name=""""),_display_(/*5.89*/elements/*5.97*/.id),format.raw/*5.100*/("""" size="40">
    """)))}),format.raw/*6.6*/("""
    """),_display_(/*7.6*/if(elements.hasErrors)/*7.28*/ {_display_(Seq[Any](format.raw/*7.30*/("""
     """),_display_(/*8.7*/for(error <- elements.errors) yield /*8.36*/ {_display_(Seq[Any](format.raw/*8.38*/("""
        """),_display_(/*9.10*/if(error.equalsIgnoreCase("VALID EMAIL REQUIRED"))/*9.60*/ {_display_(Seq[Any](format.raw/*9.62*/(""" """),format.raw/*9.63*/("""Введен неправильный email """)))}),format.raw/*9.90*/("""
    """)))}),format.raw/*10.6*/("""
    """)))}),format.raw/*11.6*/("""
"""),format.raw/*12.1*/("""</div>"""))}
  }

  def render(elements:helper.FieldElements): play.twirl.api.HtmlFormat.Appendable = apply(elements)

  def f:((helper.FieldElements) => play.twirl.api.HtmlFormat.Appendable) = (elements) => apply(elements)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/form/orderFormFieldConstructor.scala.html
                  HASH: e3e7d8848536af545031201079a78d50ca223fd5
                  MATRIX: 544->1|664->33|692->35|731->48|761->70|800->72|836->78|869->86|907->116|955->127|986->132|1058->178|1083->195|1112->197|1125->202|1152->207|1187->216|1203->224|1227->227|1274->245|1305->251|1335->273|1374->275|1406->282|1450->311|1489->313|1525->323|1583->373|1622->375|1650->376|1707->403|1743->409|1779->415|1807->416
                  LINES: 19->1|22->1|24->3|24->3|24->3|24->3|24->3|25->4|25->4|25->4|26->5|26->5|26->5|26->5|26->5|26->5|26->5|26->5|26->5|27->6|28->7|28->7|28->7|29->8|29->8|29->8|30->9|30->9|30->9|30->9|30->9|31->10|32->11|33->12
                  -- GENERATED --
              */
          