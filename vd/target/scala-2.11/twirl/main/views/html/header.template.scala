
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object header extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(page:String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {
def /*9.22*/getActive/*9.31*/(name:String):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*9.48*/("""
                        """),_display_(/*10.26*/if(page.equals(name))/*10.47*/ {_display_(Seq[Any](format.raw/*10.49*/("""
                            """),format.raw/*11.29*/("""class="active"
                        """)))}),format.raw/*12.26*/("""
                    """)))};
Seq[Any](format.raw/*1.15*/("""

"""),format.raw/*3.1*/("""<header class="top">
    <div class="container">
        <div class="grid-12">
            <nav class="menu">
                <a class="logo" href=""""),_display_(/*7.40*/routes/*7.46*/.VintageDesign.index()),format.raw/*7.68*/(""""><img class="bg" src=""""),_display_(/*7.92*/routes/*7.98*/.Assets.versioned("stylesheets/img/logo.png")),format.raw/*7.143*/(""""></a>
                <ul class="main-menu">
                    """),format.raw/*13.22*/("""

                    """),format.raw/*15.21*/("""<li """),_display_(/*15.26*/getActive("main")),format.raw/*15.43*/("""><a href=""""),_display_(/*15.54*/routes/*15.60*/.VintageDesign.index()),format.raw/*15.82*/("""">Главная</a></li>
                    <li """),_display_(/*16.26*/getActive("about")),format.raw/*16.44*/("""><a href=""""),_display_(/*16.55*/routes/*16.61*/.VintageDesign.about()),format.raw/*16.83*/("""">О нас</a></li>
                    <li """),_display_(/*17.26*/getActive("gallery")),format.raw/*17.46*/("""><a href=""""),_display_(/*17.57*/routes/*17.63*/.VintageDesign.gallery()),format.raw/*17.87*/("""">Галерея</a></li>
                    <li """),_display_(/*18.26*/getActive("storage")),format.raw/*18.46*/("""><a href=""""),_display_(/*18.57*/routes/*18.63*/.VintageDesign.storage()),format.raw/*18.87*/("""">В наличии</a></li>
                    <li """),_display_(/*19.26*/getActive("articles")),format.raw/*19.47*/("""><a href=""""),_display_(/*19.58*/routes/*19.64*/.VintageDesign.articles()),format.raw/*19.89*/("""">Новости</a></li>
                    <li """),_display_(/*20.26*/getActive("contacts")),format.raw/*20.47*/("""><a href=""""),_display_(/*20.58*/routes/*20.64*/.VintageDesign.contacts()),format.raw/*20.89*/("""">Контакты</a></li>
                    <li class="btn-zakaz"><a href=""""),_display_(/*21.53*/routes/*21.59*/.VintageDesign.order()),format.raw/*21.81*/("""">Заказать</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>"""))}
  }

  def render(page:String): play.twirl.api.HtmlFormat.Appendable = apply(page)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (page) => apply(page)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/header.scala.html
                  HASH: 9a512f986a8ed68535a85fb3507b9e99dceaf00a
                  MATRIX: 506->1|591->335|608->344|701->361|754->387|784->408|824->410|881->439|952->479|1013->14|1041->16|1216->165|1230->171|1272->193|1322->217|1336->223|1402->268|1496->501|1546->523|1578->528|1616->545|1654->556|1669->562|1712->584|1783->628|1822->646|1860->657|1875->663|1918->685|1987->727|2028->747|2066->758|2081->764|2126->788|2197->832|2238->852|2276->863|2291->869|2336->893|2409->939|2451->960|2489->971|2504->977|2550->1002|2621->1046|2663->1067|2701->1078|2716->1084|2762->1109|2861->1181|2876->1187|2919->1209
                  LINES: 19->1|21->9|21->9|23->9|24->10|24->10|24->10|25->11|26->12|28->1|30->3|34->7|34->7|34->7|34->7|34->7|34->7|36->13|38->15|38->15|38->15|38->15|38->15|38->15|39->16|39->16|39->16|39->16|39->16|40->17|40->17|40->17|40->17|40->17|41->18|41->18|41->18|41->18|41->18|42->19|42->19|42->19|42->19|42->19|43->20|43->20|43->20|43->20|43->20|44->21|44->21|44->21
                  -- GENERATED --
              */
          