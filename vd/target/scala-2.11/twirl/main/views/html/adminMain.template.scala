
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object adminMain extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(content: Html):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.17*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Панель администатора</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href=""""),_display_(/*8.51*/routes/*8.57*/.Assets.versioned("stylesheets/adminStyles.css")),format.raw/*8.105*/("""">
    <link rel="stylesheet" type="text/css" href=""""),_display_(/*9.51*/routes/*9.57*/.Assets.versioned("stylesheets/dropzone.css")),format.raw/*9.102*/("""">
    <link rel="stylesheet" type="text/css" href="stylesheets/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href=""""),_display_(/*11.51*/routes/*11.57*/.Assets.versioned("javascripts/ckeditor/samples/css/sample.css")),format.raw/*11.121*/("""">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src=""""),_display_(/*13.42*/routes/*13.48*/.Assets.versioned("javascripts/ckeditor/ckeditor.js")),format.raw/*13.101*/(""""></script>
    <script type="text/javascript" src=""""),_display_(/*14.42*/routes/*14.48*/.Assets.versioned("javascripts/ckeditor/samples/js/sample.js")),format.raw/*14.110*/(""""></script>
    <script type="text/javascript" src=""""),_display_(/*15.42*/routes/*15.48*/.Assets.versioned("javascripts/dropzone.js")),format.raw/*15.92*/(""""></script>
</head>
<body>
<div id="wrapper">
    """),_display_(/*19.6*/content),format.raw/*19.13*/("""
"""),format.raw/*20.1*/("""</div>
</body>
</html>"""))}
  }

  def render(content:Html): play.twirl.api.HtmlFormat.Appendable = apply(content)

  def f:((Html) => play.twirl.api.HtmlFormat.Appendable) = (content) => apply(content)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/adminMain.scala.html
                  HASH: f146c4e0dfc62b2c50f252788903f1a98cfd2e95
                  MATRIX: 507->1|610->16|638->18|821->175|835->181|904->229|983->282|997->288|1063->333|1224->467|1239->473|1325->537|1538->723|1553->729|1628->782|1708->835|1723->841|1807->903|1887->956|1902->962|1967->1006|2044->1057|2072->1064|2100->1065
                  LINES: 19->1|22->1|24->3|29->8|29->8|29->8|30->9|30->9|30->9|32->11|32->11|32->11|34->13|34->13|34->13|35->14|35->14|35->14|36->15|36->15|36->15|40->19|40->19|41->20
                  -- GENERATED --
              */
          