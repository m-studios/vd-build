
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template4[Seq[Category],Seq[String],Seq[Opinion],Form[Order],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(categorySeq: Seq[Category], shortDescriptions:Seq[String], opinionSeq: Seq[Opinion], orderForm:Form[Order]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.110*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),_display_(/*4.2*/header("main")),format.raw/*4.16*/("""
"""),format.raw/*5.1*/("""<div class="parallax">
    <div class="container">
        <div class="grid-12">
            <div class="parallax-p">
                <h1>Vintage Design</h1>

                <p class="parallax-h">Зеркальная мастерская</p>

                <!--<p class="parallax-text">Основой деятельности мастерской является преображение зеркала или стекла до-->
                    <!--состояния произведения искусства. Мы искренне рады смелым идеям и готовы воплотить их в жизнь.-->
                    <!--Приоритетом для себя мы выбрали создание единичных и неповторимых работ из зеркал и создание-->
                    <!--индивидуальных работ для интерьера. Там где другие разводят руками и говорят что это невозможно, мы-->
                    <!--добиваемся цели.</p>-->
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="grid-12">
        """),_display_(/*24.10*/for( i <- 0 to categorySeq.length -1) yield /*24.47*/ {_display_(Seq[Any](format.raw/*24.49*/("""
        """),_display_(/*25.10*/defining(categorySeq(i))/*25.34*/ { category =>_display_(Seq[Any](format.raw/*25.48*/("""
        """),format.raw/*26.9*/("""<div class="katalog-block">
            """),_display_(/*27.14*/if(i%2 == 0)/*27.26*/ {_display_(Seq[Any](format.raw/*27.28*/("""
            """),format.raw/*28.13*/("""<div class="katalog-img">
                <a href=""""),_display_(/*29.27*/routes/*29.33*/.VintageDesign.galleryItem(category.name)),format.raw/*29.74*/(""""><img src=""""),_display_(/*29.87*/routes/*29.93*/.Assets.versioned("images/category/" + category.photo)),format.raw/*29.147*/(""""></a>
            </div>
            """)))}),format.raw/*31.14*/("""
            """),format.raw/*32.13*/("""<div class="katalog-text">
                <div class="border"></div>
                <h2>"""),_display_(/*34.22*/category/*34.30*/.name),format.raw/*34.35*/("""</h2>

                <p>"""),_display_(/*36.21*/shortDescriptions(i)),format.raw/*36.41*/("""</p>
                <a href=""""),_display_(/*37.27*/routes/*37.33*/.VintageDesign.galleryItem(category.name)),format.raw/*37.74*/("""">Подробнее</a>
            </div>

            """),_display_(/*40.14*/if(i%2 == 1)/*40.26*/ {_display_(Seq[Any](format.raw/*40.28*/("""
            """),format.raw/*41.13*/("""<div class="katalog-img">
                <a href=""""),_display_(/*42.27*/routes/*42.33*/.VintageDesign.galleryItem(category.name)),format.raw/*42.74*/(""""><img src=""""),_display_(/*42.87*/routes/*42.93*/.Assets.versioned("images/category/" + category.photo)),format.raw/*42.147*/(""""></a>
            </div>
            """)))}),format.raw/*44.14*/("""
        """),format.raw/*45.9*/("""</div>
        """)))}),format.raw/*46.10*/("""
        """)))}),format.raw/*47.10*/("""
    """),format.raw/*48.5*/("""</div>
</div>

<div class="slider-bg-color">
    <div class="container">
        <div class="grid-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    """),_display_(/*57.22*/Option(opinionSeq)/*57.40*/.map/*57.44*/ { opinions =>_display_(Seq[Any](format.raw/*57.58*/("""
                    """),_display_(/*58.22*/opinions/*58.30*/.map/*58.34*/ { opinion =>_display_(Seq[Any](format.raw/*58.47*/("""

                    """),_display_(/*60.22*/defining(opinions.indexOf(opinion))/*60.57*/ { opinionIndex =>_display_(Seq[Any](format.raw/*60.75*/("""
                    """),_display_(/*61.22*/if(opinionIndex % 3 == 0)/*61.47*/{_display_(Seq[Any](format.raw/*61.48*/("""
                    """),_display_(/*62.22*/if(opinionIndex == 0)/*62.43*/{_display_(Seq[Any](format.raw/*62.44*/("""
                    """),format.raw/*63.21*/("""<div class="item active">
                        """)))}/*64.27*/else/*64.32*/{_display_(Seq[Any](format.raw/*64.33*/("""
                        """),format.raw/*65.25*/("""<div class="item">
                            """)))}),format.raw/*66.30*/("""
                            """),format.raw/*67.29*/("""<div class="col3 col3icon">
                                """)))}),format.raw/*68.34*/("""

                                """),format.raw/*70.33*/("""<article class="item">
                                    <img src=""""),_display_(/*71.48*/routes/*71.54*/.Assets.versioned("images/opinion/" + opinion.photo)),format.raw/*71.106*/("""">
                                    <p class="h-icon">"""),_display_(/*72.56*/opinion/*72.63*/.name),format.raw/*72.68*/("""</p>
                                    <p class="text-icon">"""),_display_(/*73.59*/opinion/*73.66*/.text),format.raw/*73.71*/("""</p>
                                </article>

                                """),_display_(/*76.34*/if(opinionIndex % 3 == 2 || opinionIndex == opinionSeq.length - 1)/*76.100*/{_display_(Seq[Any](format.raw/*76.101*/("""
                                """),format.raw/*77.33*/("""</div>
                                </div>
                                """)))}),format.raw/*79.34*/("""
                        """)))}),format.raw/*80.26*/("""
                        """)))}),format.raw/*81.26*/("""
                        """)))}),format.raw/*82.26*/("""
                    """),format.raw/*83.21*/("""</div>
                    <ol class="carousel-indicators">
                        """),_display_(/*85.26*/for(i <- 0 to opinionSeq.length - 1) yield /*85.62*/ {_display_(Seq[Any](format.raw/*85.64*/("""
                            """),_display_(/*86.30*/if(i == 0)/*86.40*/ {_display_(Seq[Any](format.raw/*86.42*/("""
                                """),format.raw/*87.33*/("""<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            """)))}/*88.31*/else/*88.36*/{_display_(Seq[Any](format.raw/*88.37*/("""
                                """),_display_(/*89.34*/if(i % 3 == 0)/*89.48*/ {_display_(Seq[Any](format.raw/*89.50*/("""
                                        """),_display_(/*90.42*/defining(Math.floor(i/3).toInt)/*90.73*/ { pageIndex =>_display_(Seq[Any](format.raw/*90.88*/("""
                                        """),format.raw/*91.41*/("""<li data-target="#carousel-example-generic" data-slide-to=""""),_display_(/*91.101*/pageIndex),format.raw/*91.110*/(""""></li>
                                    """)))}),format.raw/*92.38*/("""
                                """)))}),format.raw/*93.34*/("""
                            """)))}),format.raw/*94.30*/("""
                        """)))}),format.raw/*95.26*/("""
                    """),format.raw/*96.21*/("""</ol>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-block">
        <div class="container">
            <div class="grid-12">
                """),_display_(/*105.18*/modalOrderForm(orderForm)),format.raw/*105.43*/("""
            """),format.raw/*106.13*/("""</div>
        </div>
    </div>

    """),_display_(/*110.6*/mapAndContacts()),format.raw/*110.22*/("""
""")))}))}
  }

  def render(categorySeq:Seq[Category],shortDescriptions:Seq[String],opinionSeq:Seq[Opinion],orderForm:Form[Order]): play.twirl.api.HtmlFormat.Appendable = apply(categorySeq,shortDescriptions,opinionSeq,orderForm)

  def f:((Seq[Category],Seq[String],Seq[Opinion],Form[Order]) => play.twirl.api.HtmlFormat.Appendable) = (categorySeq,shortDescriptions,opinionSeq,orderForm) => apply(categorySeq,shortDescriptions,opinionSeq,orderForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/index.scala.html
                  HASH: ccfb4e77a8ded4479746d9b3671f91d6020a11eb
                  MATRIX: 549->1|746->109|774->112|785->116|823->118|850->120|884->134|911->135|1814->1011|1867->1048|1907->1050|1944->1060|1977->1084|2029->1098|2065->1107|2133->1148|2154->1160|2194->1162|2235->1175|2314->1227|2329->1233|2391->1274|2431->1287|2446->1293|2522->1347|2592->1386|2633->1399|2751->1490|2768->1498|2794->1503|2848->1530|2889->1550|2947->1581|2962->1587|3024->1628|3100->1677|3121->1689|3161->1691|3202->1704|3281->1756|3296->1762|3358->1803|3398->1816|3413->1822|3489->1876|3559->1915|3595->1924|3642->1940|3683->1950|3715->1955|4062->2275|4089->2293|4102->2297|4154->2311|4203->2333|4220->2341|4233->2345|4284->2358|4334->2381|4378->2416|4434->2434|4483->2456|4517->2481|4556->2482|4605->2504|4635->2525|4674->2526|4723->2547|4793->2599|4806->2604|4845->2605|4898->2630|4977->2678|5034->2707|5126->2768|5188->2802|5285->2872|5300->2878|5374->2930|5459->2988|5475->2995|5501->3000|5591->3063|5607->3070|5633->3075|5742->3157|5818->3223|5858->3224|5919->3257|6029->3336|6086->3362|6143->3388|6200->3414|6249->3435|6361->3520|6413->3556|6453->3558|6510->3588|6529->3598|6569->3600|6630->3633|6761->3746|6774->3751|6813->3752|6874->3786|6897->3800|6937->3802|7006->3844|7046->3875|7099->3890|7168->3931|7256->3991|7287->4000|7363->4045|7428->4079|7489->4109|7546->4135|7595->4156|7808->4341|7855->4366|7897->4379|7963->4418|8001->4434
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|26->5|45->24|45->24|45->24|46->25|46->25|46->25|47->26|48->27|48->27|48->27|49->28|50->29|50->29|50->29|50->29|50->29|50->29|52->31|53->32|55->34|55->34|55->34|57->36|57->36|58->37|58->37|58->37|61->40|61->40|61->40|62->41|63->42|63->42|63->42|63->42|63->42|63->42|65->44|66->45|67->46|68->47|69->48|78->57|78->57|78->57|78->57|79->58|79->58|79->58|79->58|81->60|81->60|81->60|82->61|82->61|82->61|83->62|83->62|83->62|84->63|85->64|85->64|85->64|86->65|87->66|88->67|89->68|91->70|92->71|92->71|92->71|93->72|93->72|93->72|94->73|94->73|94->73|97->76|97->76|97->76|98->77|100->79|101->80|102->81|103->82|104->83|106->85|106->85|106->85|107->86|107->86|107->86|108->87|109->88|109->88|109->88|110->89|110->89|110->89|111->90|111->90|111->90|112->91|112->91|112->91|113->92|114->93|115->94|116->95|117->96|126->105|126->105|127->106|131->110|131->110
                  -- GENERATED --
              */
          