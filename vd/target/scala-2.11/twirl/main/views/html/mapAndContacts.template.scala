
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object mapAndContacts extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.1*/("""<div class="contacts">
    <div class="container">
        <div class="grid-12">
            <div class="col2">
                <div class="item">
                    <div class="karta">
                        <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=ztC21pd6rLbh-gPY8GYnDFj1VObAQ_C2"></script>
                    </div>
                </div>
                <div class="item">
                    <ul>
                        <li><i class="fa fa-map-marker"></i>Адрес: г.Пенза, ул. Театральный проезд 1</li>
                        <li><i class="fa fa-phone"></i>Контактный телефон: +7 (927) 362-40-02</li>
                        <li><i class="fa fa-phone"></i>Контактный телефон: +7 (964) 866-88-18</li>
                        <li><i class="fa fa-at"></i>E-mail: vintagedesign&#x0040mail.ru</li>
                    </ul>
                    <div class="icons">
                        <a href="http://vk.com/mirror58"><img src=""""),_display_(/*18.69*/routes/*18.75*/.Assets.versioned("stylesheets/img/icon-1.png")),format.raw/*18.122*/(""""></a>
                        <a href="http://vk.com/mirror58"><img src=""""),_display_(/*19.69*/routes/*19.75*/.Assets.versioned("stylesheets/img/icon-3.png")),format.raw/*19.122*/(""""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>"""))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/mapAndContacts.scala.html
                  HASH: 1a874987916897c03a8facdfe1674d8b964c2559
                  MATRIX: 589->0|1624->1008|1639->1014|1708->1061|1810->1136|1825->1142|1894->1189
                  LINES: 22->1|39->18|39->18|39->18|40->19|40->19|40->19
                  -- GENERATED --
              */
          