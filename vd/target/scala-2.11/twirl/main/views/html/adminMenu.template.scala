
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object adminMenu extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Seq[String],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(itemNames:Seq[String], currentType:String):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.45*/("""

"""),_display_(/*3.2*/adminMain/*3.11*/ {_display_(Seq[Any](format.raw/*3.13*/("""
"""),format.raw/*4.1*/("""<div class="container">
    <div class="grid-12 menu-fon">
        <div class="menu-name">
            <h1>Управление сайтом</h1>
        </div>
        <div class="grid-12 menu-pad">
            <div class="grid-3">
                <div class="menu">
                    <p>Разделы</p>
                    <ul>
                        <li """),_display_(/*14.30*/if(currentType.equals("opinions"))/*14.64*/ {_display_(Seq[Any](format.raw/*14.66*/("""class="active"""")))}),format.raw/*14.81*/("""><a href=""""),_display_(/*14.92*/routes/*14.98*/.AdminVintageDesign.menu("opinions")),format.raw/*14.134*/("""">Отзывы</a></li>
                        <li """),_display_(/*15.30*/if(currentType.equals("categories"))/*15.66*/ {_display_(Seq[Any](format.raw/*15.68*/("""class="active"""")))}),format.raw/*15.83*/("""><a href=""""),_display_(/*15.94*/routes/*15.100*/.AdminVintageDesign.menu("categories")),format.raw/*15.138*/("""">Категории зеркал</a></li>
                        <li """),_display_(/*16.30*/if(currentType.equals("orderMirrors"))/*16.68*/ {_display_(Seq[Any](format.raw/*16.70*/("""class="active"""")))}),format.raw/*16.85*/("""><a href=""""),_display_(/*16.96*/routes/*16.102*/.AdminVintageDesign.menu("orderMirrors")),format.raw/*16.142*/("""">Зеркала на заказ</a></li>
                        <li """),_display_(/*17.30*/if(currentType.equals("mirrors"))/*17.63*/ {_display_(Seq[Any](format.raw/*17.65*/("""class="active"""")))}),format.raw/*17.80*/("""><a href=""""),_display_(/*17.91*/routes/*17.97*/.AdminVintageDesign.menu("mirrors")),format.raw/*17.132*/("""">Зеркала на продажу</a></li>
                    </ul>
                </div>
            </div>
            <div class="grid-9">
                <div class="element">
                    <p><a class="bt" href=""""),_display_(/*23.45*/routes/*23.51*/.AdminVintageDesign.addItemPage("opinions")),format.raw/*23.94*/("""">Добавить</a></p>
                    <table>
                        """),_display_(/*25.26*/for(itemName <- itemNames) yield /*25.52*/ {_display_(Seq[Any](format.raw/*25.54*/("""
                        """),format.raw/*26.25*/("""<tr>
                            <td class="left"><p>"""),_display_(/*27.50*/itemName),format.raw/*27.58*/("""</p></td>
                            <td><a href=""><span>Редактировать</span></a></td>
                            <td><a href=""""),_display_(/*29.43*/routes/*29.49*/.AdminVintageDesign.delete(currentType, itemName)),format.raw/*29.98*/(""""><span>Удалить</span></a></td>
                        </tr>
                        """)))}),format.raw/*31.26*/("""
                    """),format.raw/*32.21*/("""</table>
                </div>
            </div>
        </div>
    </div>
</div>
""")))}))}
  }

  def render(itemNames:Seq[String],currentType:String): play.twirl.api.HtmlFormat.Appendable = apply(itemNames,currentType)

  def f:((Seq[String],String) => play.twirl.api.HtmlFormat.Appendable) = (itemNames,currentType) => apply(itemNames,currentType)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/adminMenu.scala.html
                  HASH: 4614cb57e52df4cbbdcc3b225ecded9c8ecc8298
                  MATRIX: 521->1|652->44|680->47|697->56|736->58|763->59|1131->400|1174->434|1214->436|1260->451|1298->462|1313->468|1371->504|1445->551|1490->587|1530->589|1576->604|1614->615|1630->621|1690->659|1774->716|1821->754|1861->756|1907->771|1945->782|1961->788|2023->828|2107->885|2149->918|2189->920|2235->935|2273->946|2288->952|2345->987|2585->1200|2600->1206|2664->1249|2763->1321|2805->1347|2845->1349|2898->1374|2979->1428|3008->1436|3166->1567|3181->1573|3251->1622|3369->1709|3418->1730
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|35->14|35->14|35->14|35->14|35->14|35->14|35->14|36->15|36->15|36->15|36->15|36->15|36->15|36->15|37->16|37->16|37->16|37->16|37->16|37->16|37->16|38->17|38->17|38->17|38->17|38->17|38->17|38->17|44->23|44->23|44->23|46->25|46->25|46->25|47->26|48->27|48->27|50->29|50->29|50->29|52->31|53->32
                  -- GENERATED --
              */
          