
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object storage extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[Seq[Mirror],List[List[String]],Form[Order],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(mirrorSeq:Seq[Mirror], mirrorsPhotos:List[List[String]], orderForm:Form[Order]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.82*/("""

"""),_display_(/*3.2*/main/*3.6*/{_display_(Seq[Any](format.raw/*3.7*/("""
"""),_display_(/*4.2*/header("storage")),format.raw/*4.19*/("""
"""),format.raw/*5.1*/("""<article>
    <div class="article-border">
        <div class="container">
            <div class="grid-12">
                <div class="gallery">
                    <div class="gallery-text">
                        <h2>Зеркала в наличии</h2>
                        <p>В этом разделе представлены зеркала, которые есть в наличии.</p>
                    </div>

                            """),_display_(/*15.30*/Option(mirrorSeq)/*15.47*/.map/*15.51*/ { mirrors =>_display_(Seq[Any](format.raw/*15.64*/("""
                            """),_display_(/*16.30*/mirrors/*16.37*/.map/*16.41*/ { mirror =>_display_(Seq[Any](format.raw/*16.53*/("""

                    """),_display_(/*18.22*/defining(mirrors.indexOf(mirror))/*18.55*/ { mirrorIndex =>_display_(Seq[Any](format.raw/*18.72*/("""

                    """),_display_(/*20.22*/if(mirrorIndex % 2 == 0)/*20.46*/ {_display_(Seq[Any](format.raw/*20.48*/("""
                         """),format.raw/*21.26*/("""<div class="gallery-img-sclad">
                             <div class="col4">
                    """)))}),format.raw/*23.22*/("""
                            """),_display_(/*24.30*/defining(mirrorsPhotos(mirrorIndex))/*24.66*/ { mirrorPhotos =>_display_(Seq[Any](format.raw/*24.84*/("""
                            """),format.raw/*25.29*/("""<div class="item">
                                <a href=""""),_display_(/*26.43*/routes/*26.49*/.Assets.versioned("images/mirrors/" + mirrorPhotos(0))),format.raw/*26.103*/("""" data-lightbox="roadtrip"><img src=""""),_display_(/*26.141*/routes/*26.147*/.Assets.versioned("images/mirrors/" + mirrorPhotos(0))),format.raw/*26.201*/(""""></a>
                                <div class="col3">
                                    """),_display_(/*28.38*/for(i <- 1 to mirrorPhotos.length - 1) yield /*28.76*/ {_display_(Seq[Any](format.raw/*28.78*/("""
                                    """),format.raw/*29.37*/("""<div class="item">
                                        <a href=""""),_display_(/*30.51*/routes/*30.57*/.Assets.versioned("images/mirrors/" + mirrorPhotos(i))),format.raw/*30.111*/("""" data-lightbox="roadtrip"><img src=""""),_display_(/*30.149*/routes/*30.155*/.Assets.versioned("images/mirrors/" + mirrorPhotos(i))),format.raw/*30.209*/(""""></a>
                                    </div>
                                    """)))}),format.raw/*32.38*/("""
                                """),format.raw/*33.33*/("""</div>
                            </div>
                                 """)))}),format.raw/*35.35*/("""

                            """),format.raw/*37.29*/("""<div class="item">
                                <h3>"""),_display_(/*38.38*/mirror/*38.44*/.name),format.raw/*38.49*/("""</h3>
                                <p>"""),_display_(/*39.37*/mirror/*39.43*/.description),format.raw/*39.55*/("""</p>
                                """),_display_(/*40.34*/modalOrderForm(orderForm)),format.raw/*40.59*/("""
                            """),format.raw/*41.29*/("""</div>


                            """),_display_(/*44.30*/if(mirrorIndex % 2 == 1)/*44.54*/ {_display_(Seq[Any](format.raw/*44.56*/("""
                                 """),format.raw/*45.34*/("""</div>
                            </div>
                            """)))}),format.raw/*47.30*/("""

                            """)))}),format.raw/*49.30*/("""
                            """)))}),format.raw/*50.30*/("""
                            """)))}),format.raw/*51.30*/("""

                """),format.raw/*53.17*/("""</div>
            </div>
        </div>
    </div>
    </div>
</article>
""")))}))}
  }

  def render(mirrorSeq:Seq[Mirror],mirrorsPhotos:List[List[String]],orderForm:Form[Order]): play.twirl.api.HtmlFormat.Appendable = apply(mirrorSeq,mirrorsPhotos,orderForm)

  def f:((Seq[Mirror],List[List[String]],Form[Order]) => play.twirl.api.HtmlFormat.Appendable) = (mirrorSeq,mirrorsPhotos,orderForm) => apply(mirrorSeq,mirrorsPhotos,orderForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:34 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/storage.scala.html
                  HASH: 29f367dd0f99b9ea908bfcdb7525a4b5788faf57
                  MATRIX: 543->1|711->81|739->84|750->88|787->89|814->91|851->108|878->109|1299->503|1325->520|1338->524|1389->537|1446->567|1462->574|1475->578|1525->590|1575->613|1617->646|1672->663|1722->686|1755->710|1795->712|1849->738|1981->839|2038->869|2083->905|2139->923|2196->952|2284->1013|2299->1019|2375->1073|2441->1111|2457->1117|2533->1171|2655->1266|2709->1304|2749->1306|2814->1343|2910->1412|2925->1418|3001->1472|3067->1510|3083->1516|3159->1570|3277->1657|3338->1690|3445->1766|3503->1796|3586->1852|3601->1858|3627->1863|3696->1905|3711->1911|3744->1923|3809->1961|3855->1986|3912->2015|3977->2053|4010->2077|4050->2079|4112->2113|4214->2184|4276->2215|4337->2245|4398->2275|4444->2293
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|26->5|36->15|36->15|36->15|36->15|37->16|37->16|37->16|37->16|39->18|39->18|39->18|41->20|41->20|41->20|42->21|44->23|45->24|45->24|45->24|46->25|47->26|47->26|47->26|47->26|47->26|47->26|49->28|49->28|49->28|50->29|51->30|51->30|51->30|51->30|51->30|51->30|53->32|54->33|56->35|58->37|59->38|59->38|59->38|60->39|60->39|60->39|61->40|61->40|62->41|65->44|65->44|65->44|66->45|68->47|70->49|71->50|72->51|74->53
                  -- GENERATED --
              */
          