
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object articles extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Seq[Article],Seq[String],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(articleSeq: Seq[Article], shortDescriptions:Seq[String]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.59*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),_display_(/*4.2*/header("articles")),format.raw/*4.20*/("""
"""),format.raw/*5.1*/("""<article>
    <div class="article-border">
        <div class="container">
            <div class="grid-12">
                """),_display_(/*9.18*/Option(articleSeq)/*9.36*/.map/*9.40*/ { articles =>_display_(Seq[Any](format.raw/*9.54*/("""
                """),_display_(/*10.18*/articles/*10.26*/.map/*10.30*/ { article =>_display_(Seq[Any](format.raw/*10.43*/("""
                """),_display_(/*11.18*/defining(shortDescriptions(articles.indexOf(article)))/*11.72*/ { short =>_display_(Seq[Any](format.raw/*11.83*/("""
                """),format.raw/*12.17*/("""<div class="blog">
                    <div class="blog-img">
                        <a href=""""),_display_(/*14.35*/routes/*14.41*/.VintageDesign.article(article.id)),format.raw/*14.75*/(""""><img src=""""),_display_(/*14.88*/routes/*14.94*/.Assets.versioned("images/posts/" + article.mainPhoto)),format.raw/*14.148*/(""""></a>
                    </div>
                    <div class="blog-text">
                        <h2>"""),_display_(/*17.30*/article/*17.37*/.name),format.raw/*17.42*/("""</h2>
                        <p>"""),_display_(/*18.29*/short),format.raw/*18.34*/("""</p>
                        <p><a href=""""),_display_(/*19.38*/routes/*19.44*/.VintageDesign.article(article.id)),format.raw/*19.78*/("""">Подробнее</a></p>
                    </div>
                </div>
                """)))}),format.raw/*22.18*/("""
                """)))}),format.raw/*23.18*/("""
                """)))}),format.raw/*24.18*/("""
            """),format.raw/*25.13*/("""</div>
        </div>
    </div>
</article>
""")))}))}
  }

  def render(articleSeq:Seq[Article],shortDescriptions:Seq[String]): play.twirl.api.HtmlFormat.Appendable = apply(articleSeq,shortDescriptions)

  def f:((Seq[Article],Seq[String]) => play.twirl.api.HtmlFormat.Appendable) = (articleSeq,shortDescriptions) => apply(articleSeq,shortDescriptions)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/articles.scala.html
                  HASH: 97cfb9b1629977dcaec51ef62cab3434286aa556
                  MATRIX: 526->1|671->58|699->61|710->65|748->67|775->69|813->87|840->88|992->214|1018->232|1030->236|1081->250|1126->268|1143->276|1156->280|1207->293|1252->311|1315->365|1364->376|1409->393|1532->489|1547->495|1602->529|1642->542|1657->548|1733->602|1867->709|1883->716|1909->721|1970->755|1996->760|2065->802|2080->808|2135->842|2253->929|2302->947|2351->965|2392->978
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|26->5|30->9|30->9|30->9|30->9|31->10|31->10|31->10|31->10|32->11|32->11|32->11|33->12|35->14|35->14|35->14|35->14|35->14|35->14|38->17|38->17|38->17|39->18|39->18|40->19|40->19|40->19|43->22|44->23|45->24|46->25
                  -- GENERATED --
              */
          