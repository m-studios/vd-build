
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object modalOrderForm extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[Order],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(orderForm:Form[Order]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import views.form.ModalOrderFieldConstructorHelper._

Seq[Any](format.raw/*1.25*/("""

"""),format.raw/*4.1*/("""
"""),format.raw/*5.1*/("""<div class="button-center">
    <button class="bt" data-toggle="modal" data-target="#myModal">Заказать</button>
</div>

<div class="modal """),_display_(/*9.20*/if(!orderForm.hasErrors)/*9.44*/{_display_(Seq[Any](format.raw/*9.45*/(""" """),format.raw/*9.46*/("""fade """)))}),format.raw/*9.52*/("""" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Заказать</h4>
            </div>
            <div class="modal-body">
                """),_display_(/*17.18*/helper/*17.24*/.form(action = routes.VintageDesign.orderPost)/*17.70*/ {_display_(Seq[Any](format.raw/*17.72*/("""
                    """),format.raw/*18.21*/("""<p class="p-form">Ваше имя:* </p>
                    """),_display_(/*19.22*/helper/*19.28*/.inputText(orderForm("name"))/*19.57*/(handler = requiredField, implicitly[Lang])),format.raw/*19.100*/("""
                    """),format.raw/*20.21*/("""<p class="p-form">Ваш телефон:* </p>
                    """),_display_(/*21.22*/helper/*21.28*/.inputText(orderForm("phone"))/*21.58*/(handler = requiredField, implicitly[Lang])),format.raw/*21.101*/("""
                    """),format.raw/*22.21*/("""<p class="p-form">Ваш e-mail: </p>
                    """),_display_(/*23.22*/helper/*23.28*/.inputText(orderForm("email"))),format.raw/*23.58*/("""
                    """),format.raw/*24.21*/("""<p class="p-form">Поле для текста:* </p>
                    """),_display_(/*25.22*/helper/*25.28*/.inputText(orderForm("text"))/*25.57*/(handler = textAreaConstructor, implicitly[Lang])),format.raw/*25.106*/("""
                    """),format.raw/*26.21*/("""<p class="form-info">* - поля обязательные для заполнения</p>
                    <input class="bt" type="submit" value="Отправить">
                """)))}),format.raw/*28.18*/("""
            """),format.raw/*29.13*/("""</div>
        </div>
    </div>
</div>

"""),_display_(/*34.2*/if(orderForm.hasErrors)/*34.25*/ {_display_(Seq[Any](format.raw/*34.27*/("""
"""),format.raw/*35.1*/("""<script type="text/javascript">
        $(window).load(function()"""),format.raw/*36.34*/("""{"""),format.raw/*36.35*/("""
            """),format.raw/*37.13*/("""$('#myModal').modal('show');
        """),format.raw/*38.9*/("""}"""),format.raw/*38.10*/(""");
    </script>
""")))}))}
  }

  def render(orderForm:Form[Order]): play.twirl.api.HtmlFormat.Appendable = apply(orderForm)

  def f:((Form[Order]) => play.twirl.api.HtmlFormat.Appendable) = (orderForm) => apply(orderForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/modalOrderForm.scala.html
                  HASH: b3cbabb857704b63105d44524f81797e76956e4d
                  MATRIX: 519->1|682->24|710->80|737->81|902->220|934->244|972->245|1000->246|1036->252|1517->706|1532->712|1587->758|1627->760|1676->781|1758->836|1773->842|1811->871|1876->914|1925->935|2010->993|2025->999|2064->1029|2129->1072|2178->1093|2261->1149|2276->1155|2327->1185|2376->1206|2465->1268|2480->1274|2518->1303|2589->1352|2638->1373|2819->1523|2860->1536|2928->1578|2960->1601|3000->1603|3028->1604|3121->1669|3150->1670|3191->1683|3255->1720|3284->1721
                  LINES: 19->1|22->1|24->4|25->5|29->9|29->9|29->9|29->9|29->9|37->17|37->17|37->17|37->17|38->18|39->19|39->19|39->19|39->19|40->20|41->21|41->21|41->21|41->21|42->22|43->23|43->23|43->23|44->24|45->25|45->25|45->25|45->25|46->26|48->28|49->29|54->34|54->34|54->34|55->35|56->36|56->36|57->37|58->38|58->38
                  -- GENERATED --
              */
          