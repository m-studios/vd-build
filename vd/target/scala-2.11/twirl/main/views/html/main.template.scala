
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(content: Html):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.17*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html>
    <head>
        <title>Vintage Design</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=0, maximum-scale=0, user-scalable=no">
        <link rel="stylesheet" type="text/css" href=""""),_display_(/*9.55*/routes/*9.61*/.Assets.at("stylesheets/styles.css")),format.raw/*9.97*/("""">
        <link rel="shortcut icon" type="image/png" href="""),_display_(/*10.58*/routes/*10.64*/.Assets.at("images/favicon.png")),format.raw/*10.96*/(""">
        <script type="text/javascript" src=""""),_display_(/*11.46*/routes/*11.52*/.Assets.at("javascripts/jquery-2.1.4.min.js")),format.raw/*11.97*/(""""></script>
        <script type="text/javascript" src=""""),_display_(/*12.46*/routes/*12.52*/.Assets.at("javascripts/bootstrap.min.js")),format.raw/*12.94*/(""""></script>
        <script type="text/javascript" src=""""),_display_(/*13.46*/routes/*13.52*/.Assets.at("javascripts/lightbox.min.js")),format.raw/*13.93*/(""""></script>
        <script type="text/javascript">
		$('.carousel').carousel("""),format.raw/*15.27*/("""{"""),format.raw/*15.28*/("""
  			"""),format.raw/*16.6*/("""interval: 500
		"""),format.raw/*17.3*/("""}"""),format.raw/*17.4*/(""")

		jQuery( document ).ready(function() """),format.raw/*19.39*/("""{"""),format.raw/*19.40*/("""
			"""),format.raw/*20.4*/("""jQuery('#button_up i').mouseover( function()"""),format.raw/*20.48*/("""{"""),format.raw/*20.49*/("""
				"""),format.raw/*21.5*/("""jQuery( this ).animate("""),format.raw/*21.28*/("""{"""),format.raw/*21.29*/("""opacity: 0.65"""),format.raw/*21.42*/("""}"""),format.raw/*21.43*/(""",100);
			"""),format.raw/*22.4*/("""}"""),format.raw/*22.5*/(""").mouseout( function()"""),format.raw/*22.27*/("""{"""),format.raw/*22.28*/("""
				"""),format.raw/*23.5*/("""jQuery( this ).animate("""),format.raw/*23.28*/("""{"""),format.raw/*23.29*/("""opacity: 1"""),format.raw/*23.39*/("""}"""),format.raw/*23.40*/(""",100);
			"""),format.raw/*24.4*/("""}"""),format.raw/*24.5*/(""").click( function()"""),format.raw/*24.24*/("""{"""),format.raw/*24.25*/("""
				"""),format.raw/*25.5*/("""window.scroll(0 ,0);
				return false;
			"""),format.raw/*27.4*/("""}"""),format.raw/*27.5*/(""");

			jQuery(window).scroll(function()"""),format.raw/*29.36*/("""{"""),format.raw/*29.37*/("""
				"""),format.raw/*30.5*/("""if ( jQuery(document).scrollTop() > 0 ) """),format.raw/*30.45*/("""{"""),format.raw/*30.46*/("""
					"""),format.raw/*31.6*/("""jQuery('#button_up').fadeIn('fast');
				"""),format.raw/*32.5*/("""}"""),format.raw/*32.6*/(""" """),format.raw/*32.7*/("""else """),format.raw/*32.12*/("""{"""),format.raw/*32.13*/("""
					"""),format.raw/*33.6*/("""jQuery('#button_up').fadeOut('fast');
				"""),format.raw/*34.5*/("""}"""),format.raw/*34.6*/("""
			"""),format.raw/*35.4*/("""}"""),format.raw/*35.5*/(""");
		"""),format.raw/*36.3*/("""}"""),format.raw/*36.4*/(""");
		</script>

    </head>
    <body>
        <div id="wrapper">
            """),_display_(/*42.14*/content),format.raw/*42.21*/("""
            """),_display_(/*43.14*/footer()),format.raw/*43.22*/("""
        """),format.raw/*44.9*/("""</div>
        <div id="button_up"><i class="fa fa-angle-up"></i></div>
    </body>
</html>
"""))}
  }

  def render(content:Html): play.twirl.api.HtmlFormat.Appendable = apply(content)

  def f:((Html) => play.twirl.api.HtmlFormat.Appendable) = (content) => apply(content)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/main.scala.html
                  HASH: 6b2ebf6172eecc942615fc38cd14d79dddd1622d
                  MATRIX: 502->1|605->16|633->18|908->267|922->273|978->309|1065->369|1080->375|1133->407|1207->454|1222->460|1288->505|1372->562|1387->568|1450->610|1534->667|1549->673|1611->714|1717->792|1746->793|1779->799|1822->815|1850->816|1919->857|1948->858|1979->862|2051->906|2080->907|2112->912|2163->935|2192->936|2233->949|2262->950|2299->960|2327->961|2377->983|2406->984|2438->989|2489->1012|2518->1013|2556->1023|2585->1024|2622->1034|2650->1035|2697->1054|2726->1055|2758->1060|2827->1102|2855->1103|2922->1142|2951->1143|2983->1148|3051->1188|3080->1189|3113->1195|3181->1236|3209->1237|3237->1238|3270->1243|3299->1244|3332->1250|3401->1292|3429->1293|3460->1297|3488->1298|3520->1303|3548->1304|3654->1383|3682->1390|3723->1404|3752->1412|3788->1421
                  LINES: 19->1|22->1|24->3|30->9|30->9|30->9|31->10|31->10|31->10|32->11|32->11|32->11|33->12|33->12|33->12|34->13|34->13|34->13|36->15|36->15|37->16|38->17|38->17|40->19|40->19|41->20|41->20|41->20|42->21|42->21|42->21|42->21|42->21|43->22|43->22|43->22|43->22|44->23|44->23|44->23|44->23|44->23|45->24|45->24|45->24|45->24|46->25|48->27|48->27|50->29|50->29|51->30|51->30|51->30|52->31|53->32|53->32|53->32|53->32|53->32|54->33|55->34|55->34|56->35|56->35|57->36|57->36|63->42|63->42|64->43|64->43|65->44
                  -- GENERATED --
              */
          