
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object adminLogin extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[scala.Tuple2[String, String]],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(loginForm:Form[(String,String)]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import views.form.AdminLoginFieldConstructorHelper._

Seq[Any](format.raw/*1.35*/("""

"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/adminMain/*5.11*/ {_display_(Seq[Any](format.raw/*5.13*/("""
"""),format.raw/*6.1*/("""<div class="center-form">
    <div class="color-center-form">
        <h1>Вход в панель управления сайтом</h1>
        """),_display_(/*9.10*/helper/*9.16*/.form(action = routes.Auth.authenticate)/*9.56*/ {_display_(Seq[Any](format.raw/*9.58*/("""
        """),_display_(/*10.10*/helper/*10.16*/.inputText(loginForm("login"), 'placeholder -> "Логин")),format.raw/*10.71*/("""
        """),_display_(/*11.10*/helper/*11.16*/.inputText(loginForm("password"), 'placeholder -> "Пароль")),format.raw/*11.75*/("""
        """),format.raw/*12.9*/("""<p><i>При проблемах обращайтесь в<a href=""> службу поддержки</a></i></p>
        <input class="bt" type="submit" value="Войти">
        """)))}),format.raw/*14.10*/("""
    """),format.raw/*15.5*/("""</div>
</div>
""")))}))}
  }

  def render(loginForm:Form[scala.Tuple2[String, String]]): play.twirl.api.HtmlFormat.Appendable = apply(loginForm)

  def f:((Form[scala.Tuple2[String, String]]) => play.twirl.api.HtmlFormat.Appendable) = (loginForm) => apply(loginForm)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/adminLogin.scala.html
                  HASH: b7d4f451d8da6c5a68f985bce2b4205a62a13e3f
                  MATRIX: 538->1|711->34|739->90|766->92|783->101|822->103|849->104|995->224|1009->230|1057->270|1096->272|1133->282|1148->288|1224->343|1261->353|1276->359|1356->418|1392->427|1561->565|1593->570
                  LINES: 19->1|22->1|24->4|25->5|25->5|25->5|26->6|29->9|29->9|29->9|29->9|30->10|30->10|30->10|31->11|31->11|31->11|32->12|34->14|35->15
                  -- GENERATED --
              */
          