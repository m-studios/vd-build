
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object contacts extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[Order],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(form:Form[Order]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.20*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),_display_(/*4.2*/header("contacts")),format.raw/*4.20*/("""

"""),format.raw/*6.1*/("""<article>
    <div class="karta-bg-color">
        <div class="container">
            <div class="grid-12">
                <h2>Как нас найти</h2>
                <div class="karta">
                    <script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=ztC21pd6rLbh-gPY8GYnDFj1VObAQ_C2"></script>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-bg-color">
        <div class="container">
            <div class="grid-12">
                <div class="contact">
                    <div class="col3">
                        <div class="item">
                            <i class="fa fa-envelope-o"></i>

                            <p>vintagedesign&#x0040mail.ru</p>
                        </div>
                        <div class="item">
                            <i class="fa fa-mobile"></i>
                            <p>+7 (927) 362-40-02</p>
                            <p>+7 (964) 866-88-18</p>
                        </div>
                        <div class="item">
                            <i class="fa fa-map-marker"></i>

                            <p>г.Пенза, ул. Театральный проезд 1</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    """),_display_(/*42.6*/orderForm("Связяться с нами", form)),format.raw/*42.41*/("""
"""),format.raw/*43.1*/("""</article>
""")))}))}
  }

  def render(form:Form[Order]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Order]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/contacts.scala.html
                  HASH: 2681daf53c832e56db5797fa1bf9b03531d4cf34
                  MATRIX: 513->1|619->19|647->22|658->26|696->28|723->30|761->48|789->50|2164->1399|2220->1434|2248->1435
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|27->6|63->42|63->42|64->43
                  -- GENERATED --
              */
          