
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object goodOrder extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Order,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(order:Order):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.15*/("""

"""),_display_(/*3.2*/main/*3.6*/ {_display_(Seq[Any](format.raw/*3.8*/("""
"""),_display_(/*4.2*/header("")),format.raw/*4.12*/("""
"""),format.raw/*5.1*/("""<h2>"""),_display_(/*5.6*/order/*5.11*/.name),format.raw/*5.16*/("""</h2>
<h2>"""),_display_(/*6.6*/order/*6.11*/.phone),format.raw/*6.17*/("""</h2>
<h2>"""),_display_(/*7.6*/order/*7.11*/.email),format.raw/*7.17*/("""</h2>
<h2>"""),_display_(/*8.6*/order/*8.11*/.text),format.raw/*8.16*/("""</h2>
<p class="success">Ваше сообщение отправлено</p>
""")))}))}
  }

  def render(order:Order): play.twirl.api.HtmlFormat.Appendable = apply(order)

  def f:((Order) => play.twirl.api.HtmlFormat.Appendable) = (order) => apply(order)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/goodOrder.scala.html
                  HASH: 540d25f75af30ae3b4ed4c5c046bdf36b82cbf17
                  MATRIX: 508->1|609->14|637->17|648->21|686->23|713->25|743->35|770->36|800->41|813->46|838->51|874->62|887->67|913->73|949->84|962->89|988->95|1024->106|1037->111|1062->116
                  LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|26->5|26->5|26->5|26->5|27->6|27->6|27->6|28->7|28->7|28->7|29->8|29->8|29->8
                  -- GENERATED --
              */
          