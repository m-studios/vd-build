
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._

/**/
object footer extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.1*/("""<div id="footer">
    <div class="copy">
        <div class="container">
            <div class="grid-12">
                <p>Все права сохранены, 2015 Vintage Design</p>
                <a class="m-studio" href="http://m-studios.ru" title="Мы делаем сайты" target="_blank"><img src=""""),_display_(/*6.115*/routes/*6.121*/.Assets.versioned("stylesheets/img/logo-st.png")),format.raw/*6.169*/(""""></a>
            </div>
        </div>
    </div>
</div>"""))}
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Sat Oct 03 22:39:33 MSK 2015
                  SOURCE: /home/sergeyshpadyrev/Documents/Projects/vd-build/vd/app/views/footer.scala.html
                  HASH: 1c8c1f393fe2364637bccd9f65e14688e0b049b5
                  MATRIX: 581->0|893->285|908->291|977->339
                  LINES: 22->1|27->6|27->6|27->6
                  -- GENERATED --
              */
          