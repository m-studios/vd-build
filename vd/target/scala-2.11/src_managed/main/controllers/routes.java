// @SOURCE:/home/sergeyshpadyrev/Documents/Projects/vd-build/vd/conf/routes
// @HASH:b71375e0dba11fbcfd8b8f3283398139d8619d7f
// @DATE:Sat Oct 03 22:39:33 MSK 2015

package controllers;

public class routes {
public static final controllers.ReverseAuth Auth = new controllers.ReverseAuth();
public static final controllers.ReverseAdminVintageDesign AdminVintageDesign = new controllers.ReverseAdminVintageDesign();
public static final controllers.ReverseVintageDesign VintageDesign = new controllers.ReverseVintageDesign();
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();

public static class javascript {
public static final controllers.javascript.ReverseAuth Auth = new controllers.javascript.ReverseAuth();
public static final controllers.javascript.ReverseAdminVintageDesign AdminVintageDesign = new controllers.javascript.ReverseAdminVintageDesign();
public static final controllers.javascript.ReverseVintageDesign VintageDesign = new controllers.javascript.ReverseVintageDesign();
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
}
          

public static class ref {
public static final controllers.ref.ReverseAuth Auth = new controllers.ref.ReverseAuth();
public static final controllers.ref.ReverseAdminVintageDesign AdminVintageDesign = new controllers.ref.ReverseAdminVintageDesign();
public static final controllers.ref.ReverseVintageDesign VintageDesign = new controllers.ref.ReverseVintageDesign();
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
}
          
}
          