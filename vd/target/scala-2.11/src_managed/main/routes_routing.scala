// @SOURCE:/home/sergeyshpadyrev/Documents/Projects/vd-build/vd/conf/routes
// @HASH:b71375e0dba11fbcfd8b8f3283398139d8619d7f
// @DATE:Sat Oct 03 22:39:33 MSK 2015


import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset

import Router.queryString

object Routes extends Router.Routes {

import ReverseRouteContext.empty

private var _prefix = "/"

def setPrefix(prefix: String): Unit = {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:7
private[this] lazy val controllers_VintageDesign_index0_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
private[this] lazy val controllers_VintageDesign_index0_invoker = createInvoker(
controllers.VintageDesign.index,
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "index", Nil,"GET", """ Default path will just redirect to the employee list""", Routes.prefix + """"""))
        

// @LINE:9
private[this] lazy val controllers_VintageDesign_about1_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("about"))))
private[this] lazy val controllers_VintageDesign_about1_invoker = createInvoker(
controllers.VintageDesign.about,
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "about", Nil,"GET", """""", Routes.prefix + """about"""))
        

// @LINE:11
private[this] lazy val controllers_VintageDesign_gallery2_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("gallery"))))
private[this] lazy val controllers_VintageDesign_gallery2_invoker = createInvoker(
controllers.VintageDesign.gallery,
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "gallery", Nil,"GET", """""", Routes.prefix + """gallery"""))
        

// @LINE:13
private[this] lazy val controllers_VintageDesign_galleryItem3_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("galleryItem/"),DynamicPart("name", """[^/]+""",true))))
private[this] lazy val controllers_VintageDesign_galleryItem3_invoker = createInvoker(
controllers.VintageDesign.galleryItem(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "galleryItem", Seq(classOf[String]),"GET", """""", Routes.prefix + """galleryItem/$name<[^/]+>"""))
        

// @LINE:15
private[this] lazy val controllers_VintageDesign_storage4_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("storage"))))
private[this] lazy val controllers_VintageDesign_storage4_invoker = createInvoker(
controllers.VintageDesign.storage,
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "storage", Nil,"GET", """""", Routes.prefix + """storage"""))
        

// @LINE:17
private[this] lazy val controllers_VintageDesign_contacts5_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("contacts"))))
private[this] lazy val controllers_VintageDesign_contacts5_invoker = createInvoker(
controllers.VintageDesign.contacts,
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "contacts", Nil,"GET", """""", Routes.prefix + """contacts"""))
        

// @LINE:19
private[this] lazy val controllers_VintageDesign_articles6_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("articles"))))
private[this] lazy val controllers_VintageDesign_articles6_invoker = createInvoker(
controllers.VintageDesign.articles,
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "articles", Nil,"GET", """""", Routes.prefix + """articles"""))
        

// @LINE:21
private[this] lazy val controllers_VintageDesign_article7_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("article/"),DynamicPart("id", """[^/]+""",true))))
private[this] lazy val controllers_VintageDesign_article7_invoker = createInvoker(
controllers.VintageDesign.article(fakeValue[Long]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "article", Seq(classOf[Long]),"GET", """""", Routes.prefix + """article/$id<[^/]+>"""))
        

// @LINE:23
private[this] lazy val controllers_VintageDesign_order8_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("order"))))
private[this] lazy val controllers_VintageDesign_order8_invoker = createInvoker(
controllers.VintageDesign.order,
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "order", Nil,"GET", """""", Routes.prefix + """order"""))
        

// @LINE:25
private[this] lazy val controllers_VintageDesign_orderPost9_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("order"))))
private[this] lazy val controllers_VintageDesign_orderPost9_invoker = createInvoker(
controllers.VintageDesign.orderPost,
HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "orderPost", Nil,"POST", """""", Routes.prefix + """order"""))
        

// @LINE:27
private[this] lazy val controllers_Assets_at10_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/at/"),DynamicPart("file", """.+""",false))))
private[this] lazy val controllers_Assets_at10_invoker = createInvoker(
controllers.Assets.at(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """assets/at/$file<.+>"""))
        

// @LINE:29
private[this] lazy val controllers_Assets_versioned11_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
private[this] lazy val controllers_Assets_versioned11_invoker = createInvoker(
controllers.Assets.versioned(fakeValue[String], fakeValue[Asset]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "versioned", Seq(classOf[String], classOf[Asset]),"GET", """""", Routes.prefix + """assets/$file<.+>"""))
        

// @LINE:31
private[this] lazy val controllers_AdminVintageDesign_testOpinion12_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/opinion"))))
private[this] lazy val controllers_AdminVintageDesign_testOpinion12_invoker = createInvoker(
controllers.AdminVintageDesign.testOpinion,
HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "testOpinion", Nil,"GET", """""", Routes.prefix + """admin/opinion"""))
        

// @LINE:33
private[this] lazy val controllers_AdminVintageDesign_postOpinion13_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/postOpinion"))))
private[this] lazy val controllers_AdminVintageDesign_postOpinion13_invoker = createInvoker(
controllers.AdminVintageDesign.postOpinion,
HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "postOpinion", Nil,"POST", """""", Routes.prefix + """admin/postOpinion"""))
        

// @LINE:35
private[this] lazy val controllers_AdminVintageDesign_index14_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin"))))
private[this] lazy val controllers_AdminVintageDesign_index14_invoker = createInvoker(
controllers.AdminVintageDesign.index,
HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "index", Nil,"GET", """""", Routes.prefix + """admin"""))
        

// @LINE:37
private[this] lazy val controllers_Auth_login15_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/login"))))
private[this] lazy val controllers_Auth_login15_invoker = createInvoker(
controllers.Auth.login,
HandlerDef(this.getClass.getClassLoader, "", "controllers.Auth", "login", Nil,"GET", """""", Routes.prefix + """admin/login"""))
        

// @LINE:39
private[this] lazy val controllers_Auth_authenticate16_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/auth"))))
private[this] lazy val controllers_Auth_authenticate16_invoker = createInvoker(
controllers.Auth.authenticate,
HandlerDef(this.getClass.getClassLoader, "", "controllers.Auth", "authenticate", Nil,"POST", """""", Routes.prefix + """admin/auth"""))
        

// @LINE:41
private[this] lazy val controllers_AdminVintageDesign_menu17_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/menu/"),DynamicPart("menuType", """[^/]+""",true))))
private[this] lazy val controllers_AdminVintageDesign_menu17_invoker = createInvoker(
controllers.AdminVintageDesign.menu(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "menu", Seq(classOf[String]),"GET", """""", Routes.prefix + """admin/menu/$menuType<[^/]+>"""))
        

// @LINE:43
private[this] lazy val controllers_AdminVintageDesign_delete18_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/delete/"),DynamicPart("menuType", """[^/]+""",true),StaticPart("/"),DynamicPart("name", """[^/]+""",true))))
private[this] lazy val controllers_AdminVintageDesign_delete18_invoker = createInvoker(
controllers.AdminVintageDesign.delete(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "delete", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """admin/delete/$menuType<[^/]+>/$name<[^/]+>"""))
        

// @LINE:45
private[this] lazy val controllers_AdminVintageDesign_addItemPage19_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/addItem/"),DynamicPart("menuType", """[^/]+""",true))))
private[this] lazy val controllers_AdminVintageDesign_addItemPage19_invoker = createInvoker(
controllers.AdminVintageDesign.addItemPage(fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "addItemPage", Seq(classOf[String]),"GET", """""", Routes.prefix + """admin/addItem/$menuType<[^/]+>"""))
        
def documentation = List(("""GET""", prefix,"""controllers.VintageDesign.index"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """about""","""controllers.VintageDesign.about"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """gallery""","""controllers.VintageDesign.gallery"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """galleryItem/$name<[^/]+>""","""controllers.VintageDesign.galleryItem(name:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """storage""","""controllers.VintageDesign.storage"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """contacts""","""controllers.VintageDesign.contacts"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """articles""","""controllers.VintageDesign.articles"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """article/$id<[^/]+>""","""controllers.VintageDesign.article(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """order""","""controllers.VintageDesign.order"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """order""","""controllers.VintageDesign.orderPost"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/at/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.versioned(path:String = "/public", file:Asset)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/opinion""","""controllers.AdminVintageDesign.testOpinion"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/postOpinion""","""controllers.AdminVintageDesign.postOpinion"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin""","""controllers.AdminVintageDesign.index"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/login""","""controllers.Auth.login"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/auth""","""controllers.Auth.authenticate"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/menu/$menuType<[^/]+>""","""controllers.AdminVintageDesign.menu(menuType:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/delete/$menuType<[^/]+>/$name<[^/]+>""","""controllers.AdminVintageDesign.delete(menuType:String, name:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/addItem/$menuType<[^/]+>""","""controllers.AdminVintageDesign.addItemPage(menuType:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]]
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:7
case controllers_VintageDesign_index0_route(params) => {
   call { 
        controllers_VintageDesign_index0_invoker.call(controllers.VintageDesign.index)
   }
}
        

// @LINE:9
case controllers_VintageDesign_about1_route(params) => {
   call { 
        controllers_VintageDesign_about1_invoker.call(controllers.VintageDesign.about)
   }
}
        

// @LINE:11
case controllers_VintageDesign_gallery2_route(params) => {
   call { 
        controllers_VintageDesign_gallery2_invoker.call(controllers.VintageDesign.gallery)
   }
}
        

// @LINE:13
case controllers_VintageDesign_galleryItem3_route(params) => {
   call(params.fromPath[String]("name", None)) { (name) =>
        controllers_VintageDesign_galleryItem3_invoker.call(controllers.VintageDesign.galleryItem(name))
   }
}
        

// @LINE:15
case controllers_VintageDesign_storage4_route(params) => {
   call { 
        controllers_VintageDesign_storage4_invoker.call(controllers.VintageDesign.storage)
   }
}
        

// @LINE:17
case controllers_VintageDesign_contacts5_route(params) => {
   call { 
        controllers_VintageDesign_contacts5_invoker.call(controllers.VintageDesign.contacts)
   }
}
        

// @LINE:19
case controllers_VintageDesign_articles6_route(params) => {
   call { 
        controllers_VintageDesign_articles6_invoker.call(controllers.VintageDesign.articles)
   }
}
        

// @LINE:21
case controllers_VintageDesign_article7_route(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_VintageDesign_article7_invoker.call(controllers.VintageDesign.article(id))
   }
}
        

// @LINE:23
case controllers_VintageDesign_order8_route(params) => {
   call { 
        controllers_VintageDesign_order8_invoker.call(controllers.VintageDesign.order)
   }
}
        

// @LINE:25
case controllers_VintageDesign_orderPost9_route(params) => {
   call { 
        controllers_VintageDesign_orderPost9_invoker.call(controllers.VintageDesign.orderPost)
   }
}
        

// @LINE:27
case controllers_Assets_at10_route(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at10_invoker.call(controllers.Assets.at(path, file))
   }
}
        

// @LINE:29
case controllers_Assets_versioned11_route(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned11_invoker.call(controllers.Assets.versioned(path, file))
   }
}
        

// @LINE:31
case controllers_AdminVintageDesign_testOpinion12_route(params) => {
   call { 
        controllers_AdminVintageDesign_testOpinion12_invoker.call(controllers.AdminVintageDesign.testOpinion)
   }
}
        

// @LINE:33
case controllers_AdminVintageDesign_postOpinion13_route(params) => {
   call { 
        controllers_AdminVintageDesign_postOpinion13_invoker.call(controllers.AdminVintageDesign.postOpinion)
   }
}
        

// @LINE:35
case controllers_AdminVintageDesign_index14_route(params) => {
   call { 
        controllers_AdminVintageDesign_index14_invoker.call(controllers.AdminVintageDesign.index)
   }
}
        

// @LINE:37
case controllers_Auth_login15_route(params) => {
   call { 
        controllers_Auth_login15_invoker.call(controllers.Auth.login)
   }
}
        

// @LINE:39
case controllers_Auth_authenticate16_route(params) => {
   call { 
        controllers_Auth_authenticate16_invoker.call(controllers.Auth.authenticate)
   }
}
        

// @LINE:41
case controllers_AdminVintageDesign_menu17_route(params) => {
   call(params.fromPath[String]("menuType", None)) { (menuType) =>
        controllers_AdminVintageDesign_menu17_invoker.call(controllers.AdminVintageDesign.menu(menuType))
   }
}
        

// @LINE:43
case controllers_AdminVintageDesign_delete18_route(params) => {
   call(params.fromPath[String]("menuType", None), params.fromPath[String]("name", None)) { (menuType, name) =>
        controllers_AdminVintageDesign_delete18_invoker.call(controllers.AdminVintageDesign.delete(menuType, name))
   }
}
        

// @LINE:45
case controllers_AdminVintageDesign_addItemPage19_route(params) => {
   call(params.fromPath[String]("menuType", None)) { (menuType) =>
        controllers_AdminVintageDesign_addItemPage19_invoker.call(controllers.AdminVintageDesign.addItemPage(menuType))
   }
}
        
}

}
     