// @SOURCE:/home/sergeyshpadyrev/Documents/Projects/vd-build/vd/conf/routes
// @HASH:b71375e0dba11fbcfd8b8f3283398139d8619d7f
// @DATE:Sat Oct 03 22:39:33 MSK 2015

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset

import Router.queryString


// @LINE:45
// @LINE:43
// @LINE:41
// @LINE:39
// @LINE:37
// @LINE:35
// @LINE:33
// @LINE:31
// @LINE:29
// @LINE:27
// @LINE:25
// @LINE:23
// @LINE:21
// @LINE:19
// @LINE:17
// @LINE:15
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:7
package controllers {

// @LINE:39
// @LINE:37
class ReverseAuth {


// @LINE:39
def authenticate(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "admin/auth")
}
                        

// @LINE:37
def login(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "admin/login")
}
                        

}
                          

// @LINE:45
// @LINE:43
// @LINE:41
// @LINE:35
// @LINE:33
// @LINE:31
class ReverseAdminVintageDesign {


// @LINE:41
def menu(menuType:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "admin/menu/" + implicitly[PathBindable[String]].unbind("menuType", dynamicString(menuType)))
}
                        

// @LINE:45
def addItemPage(menuType:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "admin/addItem/" + implicitly[PathBindable[String]].unbind("menuType", dynamicString(menuType)))
}
                        

// @LINE:33
def postOpinion(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "admin/postOpinion")
}
                        

// @LINE:43
def delete(menuType:String, name:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "admin/delete/" + implicitly[PathBindable[String]].unbind("menuType", dynamicString(menuType)) + "/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
}
                        

// @LINE:31
def testOpinion(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "admin/opinion")
}
                        

// @LINE:35
def index(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "admin")
}
                        

}
                          

// @LINE:25
// @LINE:23
// @LINE:21
// @LINE:19
// @LINE:17
// @LINE:15
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:7
class ReverseVintageDesign {


// @LINE:19
def articles(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "articles")
}
                        

// @LINE:13
def galleryItem(name:String): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "galleryItem/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
}
                        

// @LINE:21
def article(id:Long): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "article/" + implicitly[PathBindable[Long]].unbind("id", id))
}
                        

// @LINE:9
def about(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "about")
}
                        

// @LINE:25
def orderPost(): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + { _defaultPrefix } + "order")
}
                        

// @LINE:15
def storage(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "storage")
}
                        

// @LINE:23
def order(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "order")
}
                        

// @LINE:7
def index(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix)
}
                        

// @LINE:17
def contacts(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "contacts")
}
                        

// @LINE:11
def gallery(): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + { _defaultPrefix } + "gallery")
}
                        

}
                          

// @LINE:29
// @LINE:27
class ReverseAssets {


// @LINE:27
def at(file:String): Call = {
   implicit val _rrc = new ReverseRouteContext(Map(("path", "/public")))
   Call("GET", _prefix + { _defaultPrefix } + "assets/at/" + implicitly[PathBindable[String]].unbind("file", file))
}
                        

// @LINE:29
def versioned(file:Asset): Call = {
   implicit val _rrc = new ReverseRouteContext(Map(("path", "/public")))
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[Asset]].unbind("file", file))
}
                        

}
                          
}
                  


// @LINE:45
// @LINE:43
// @LINE:41
// @LINE:39
// @LINE:37
// @LINE:35
// @LINE:33
// @LINE:31
// @LINE:29
// @LINE:27
// @LINE:25
// @LINE:23
// @LINE:21
// @LINE:19
// @LINE:17
// @LINE:15
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:7
package controllers.javascript {
import ReverseRouteContext.empty

// @LINE:39
// @LINE:37
class ReverseAuth {


// @LINE:39
def authenticate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Auth.authenticate",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/auth"})
      }
   """
)
                        

// @LINE:37
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Auth.login",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/login"})
      }
   """
)
                        

}
              

// @LINE:45
// @LINE:43
// @LINE:41
// @LINE:35
// @LINE:33
// @LINE:31
class ReverseAdminVintageDesign {


// @LINE:41
def menu : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.AdminVintageDesign.menu",
   """
      function(menuType) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/menu/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("menuType", encodeURIComponent(menuType))})
      }
   """
)
                        

// @LINE:45
def addItemPage : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.AdminVintageDesign.addItemPage",
   """
      function(menuType) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addItem/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("menuType", encodeURIComponent(menuType))})
      }
   """
)
                        

// @LINE:33
def postOpinion : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.AdminVintageDesign.postOpinion",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/postOpinion"})
      }
   """
)
                        

// @LINE:43
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.AdminVintageDesign.delete",
   """
      function(menuType,name) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/delete/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("menuType", encodeURIComponent(menuType)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
   """
)
                        

// @LINE:31
def testOpinion : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.AdminVintageDesign.testOpinion",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/opinion"})
      }
   """
)
                        

// @LINE:35
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.AdminVintageDesign.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin"})
      }
   """
)
                        

}
              

// @LINE:25
// @LINE:23
// @LINE:21
// @LINE:19
// @LINE:17
// @LINE:15
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:7
class ReverseVintageDesign {


// @LINE:19
def articles : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.articles",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "articles"})
      }
   """
)
                        

// @LINE:13
def galleryItem : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.galleryItem",
   """
      function(name) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "galleryItem/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
   """
)
                        

// @LINE:21
def article : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.article",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "article/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:9
def about : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.about",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "about"})
      }
   """
)
                        

// @LINE:25
def orderPost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.orderPost",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "order"})
      }
   """
)
                        

// @LINE:15
def storage : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.storage",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "storage"})
      }
   """
)
                        

// @LINE:23
def order : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.order",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "order"})
      }
   """
)
                        

// @LINE:7
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:17
def contacts : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.contacts",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "contacts"})
      }
   """
)
                        

// @LINE:11
def gallery : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.VintageDesign.gallery",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "gallery"})
      }
   """
)
                        

}
              

// @LINE:29
// @LINE:27
class ReverseAssets {


// @LINE:27
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/at/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        

// @LINE:29
def versioned : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.versioned",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[Asset]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        

}
              
}
        


// @LINE:45
// @LINE:43
// @LINE:41
// @LINE:39
// @LINE:37
// @LINE:35
// @LINE:33
// @LINE:31
// @LINE:29
// @LINE:27
// @LINE:25
// @LINE:23
// @LINE:21
// @LINE:19
// @LINE:17
// @LINE:15
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:7
package controllers.ref {


// @LINE:39
// @LINE:37
class ReverseAuth {


// @LINE:39
def authenticate(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Auth.authenticate(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Auth", "authenticate", Seq(), "POST", """""", _prefix + """admin/auth""")
)
                      

// @LINE:37
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Auth.login(), HandlerDef(this.getClass.getClassLoader, "", "controllers.Auth", "login", Seq(), "GET", """""", _prefix + """admin/login""")
)
                      

}
                          

// @LINE:45
// @LINE:43
// @LINE:41
// @LINE:35
// @LINE:33
// @LINE:31
class ReverseAdminVintageDesign {


// @LINE:41
def menu(menuType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.AdminVintageDesign.menu(menuType), HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "menu", Seq(classOf[String]), "GET", """""", _prefix + """admin/menu/$menuType<[^/]+>""")
)
                      

// @LINE:45
def addItemPage(menuType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.AdminVintageDesign.addItemPage(menuType), HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "addItemPage", Seq(classOf[String]), "GET", """""", _prefix + """admin/addItem/$menuType<[^/]+>""")
)
                      

// @LINE:33
def postOpinion(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.AdminVintageDesign.postOpinion(), HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "postOpinion", Seq(), "POST", """""", _prefix + """admin/postOpinion""")
)
                      

// @LINE:43
def delete(menuType:String, name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.AdminVintageDesign.delete(menuType, name), HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "delete", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """admin/delete/$menuType<[^/]+>/$name<[^/]+>""")
)
                      

// @LINE:31
def testOpinion(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.AdminVintageDesign.testOpinion(), HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "testOpinion", Seq(), "GET", """""", _prefix + """admin/opinion""")
)
                      

// @LINE:35
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.AdminVintageDesign.index(), HandlerDef(this.getClass.getClassLoader, "", "controllers.AdminVintageDesign", "index", Seq(), "GET", """""", _prefix + """admin""")
)
                      

}
                          

// @LINE:25
// @LINE:23
// @LINE:21
// @LINE:19
// @LINE:17
// @LINE:15
// @LINE:13
// @LINE:11
// @LINE:9
// @LINE:7
class ReverseVintageDesign {


// @LINE:19
def articles(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.articles(), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "articles", Seq(), "GET", """""", _prefix + """articles""")
)
                      

// @LINE:13
def galleryItem(name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.galleryItem(name), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "galleryItem", Seq(classOf[String]), "GET", """""", _prefix + """galleryItem/$name<[^/]+>""")
)
                      

// @LINE:21
def article(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.article(id), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "article", Seq(classOf[Long]), "GET", """""", _prefix + """article/$id<[^/]+>""")
)
                      

// @LINE:9
def about(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.about(), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "about", Seq(), "GET", """""", _prefix + """about""")
)
                      

// @LINE:25
def orderPost(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.orderPost(), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "orderPost", Seq(), "POST", """""", _prefix + """order""")
)
                      

// @LINE:15
def storage(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.storage(), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "storage", Seq(), "GET", """""", _prefix + """storage""")
)
                      

// @LINE:23
def order(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.order(), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "order", Seq(), "GET", """""", _prefix + """order""")
)
                      

// @LINE:7
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.index(), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "index", Seq(), "GET", """ Default path will just redirect to the employee list""", _prefix + """""")
)
                      

// @LINE:17
def contacts(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.contacts(), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "contacts", Seq(), "GET", """""", _prefix + """contacts""")
)
                      

// @LINE:11
def gallery(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.VintageDesign.gallery(), HandlerDef(this.getClass.getClassLoader, "", "controllers.VintageDesign", "gallery", Seq(), "GET", """""", _prefix + """gallery""")
)
                      

}
                          

// @LINE:29
// @LINE:27
class ReverseAssets {


// @LINE:27
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """assets/at/$file<.+>""")
)
                      

// @LINE:29
def versioned(path:String, file:Asset): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.versioned(path, file), HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "versioned", Seq(classOf[String], classOf[Asset]), "GET", """""", _prefix + """assets/$file<.+>""")
)
                      

}
                          
}
        
    