package controllers

import play.api.data._
import play.api.data.Forms._
import play.api.mvc.{Security, Action, Controller}
import views.html

/**
 * Created by Sergey on 03.08.2015.
 */
object Auth extends Controller {

  val loginForm = Form(
    tuple(
      "login" -> nonEmptyText,
      "password" -> text
    ) verifying ("Invalid email or password", result => result match {
      case (login, password) => check(login, password)
    })
  )

  def check(username: String, password: String) = {
    (username == "admin" && password == "sfdsf5llfgdflg4435")
  }

  def login = Action { implicit request =>
    Ok(html.adminLogin(loginForm))
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.adminLogin(formWithErrors)),
      user => Redirect(routes.AdminVintageDesign.index).withSession(Security.username -> user._1)
    )
  }

  def logout = Action {
    Redirect(routes.Auth.login).withNewSession.flashing(
      "success" -> "You are now logged out."
    )
  }
}