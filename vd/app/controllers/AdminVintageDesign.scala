package controllers

import java.io.File
import java.nio.file.{Paths, Files}
import java.util.concurrent.TimeoutException

import controllers.VintageDesign._
import models._
import play.api.Logger
import play.api.mvc.{Controller, AnyContent, Action}
import play.mvc.Security
import views.html

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Sergey on 08.07.2015.
 */
object AdminVintageDesign extends Controller with Secured {

  val OPINIONS: String = "opinions"
  val CATEGORIES: String = "categories"
  val ORDER_MIRRORS: String = "orderMirrors"
  val MIRRORS: String = "mirrors"
  val ARTICLES: String = "articles"

  def index = menu(OPINIONS)

  def menu(menuType: String) = withAuth { username => implicit request =>
    menuType match {
      case s if menuType.startsWith(OPINIONS) => opinionsMenu()
      case s if menuType.startsWith(CATEGORIES) => categoriesMenu()
      case s if menuType.startsWith(ORDER_MIRRORS) => orderMirrorsMenu()
      case s if menuType.startsWith(MIRRORS) => mirrorsMenu()
    }
  }

  def opinionsMenu() = {
    val items: ListBuffer[String] = new ListBuffer[String]
    val opinionsData = OpinionDao.list()

    opinionsData.map {
      opinions => {
        for (opinion <- opinions) {
          val opinionName = opinion.name
          items += opinionName
        }

        Ok {
          html.adminMenu(items.toList, OPINIONS)
        }
      }
    }
  }

  def categoriesMenu() = {
    val items: ListBuffer[String] = new ListBuffer[String]
    val categoryData = CategoryDao.list()

    categoryData.map {
      categories => {
        for (category <- categories) {
          val categoryName = category.name
          items += categoryName
        }

        Ok {
          html.adminMenu(items.toList, CATEGORIES)
        }
      }
    }
  }

  def orderMirrorsMenu() = {
    val items: ListBuffer[String] = new ListBuffer[String]
    val orderMirrorData = OrderMirrorDao.list()

    orderMirrorData.map {
      orderMirrors => {
        for (orderMirror <- orderMirrors) {
          val orderMirrorName = orderMirror.name
          items += orderMirrorName
        }

        Ok {
          html.adminMenu(items.toList, ORDER_MIRRORS)
        }
      }
    }
  }

  def mirrorsMenu() = {
    val items: ListBuffer[String] = new ListBuffer[String]
    val mirrorData = MirrorDao.list()

    mirrorData.map {
      mirrors => {
        for (mirror <- mirrors) {
          val mirrorName = mirror.name
          items += mirrorName
        }

        Ok {
          html.adminMenu(items.toList, MIRRORS)
        }
      }
    }
  }

  def delete(menuType: String, name: String) = withAuth {
    username => implicit request => {
      val deleteResult = menuType match {
        case s if menuType.startsWith(OPINIONS) => OpinionDao.delete(name)
        case s if menuType.startsWith(CATEGORIES) => CategoryDao.delete(name)
        case s if menuType.startsWith(ORDER_MIRRORS) => OrderMirrorDao.delete(name)
        case s if menuType.startsWith(MIRRORS) => MirrorDao.delete(name)
      }

      deleteResult.map {
        deleted => {
          Redirect(routes.AdminVintageDesign.menu(menuType))
        }
      }
    }
  }

  def addItemPage(menuType: String) = withAuthImmediatly {
    username => implicit request => {
      Ok {
        menuType match {
          case s if menuType.startsWith(OPINIONS) => html.addOpinion(VintageDesignForms.opinionForm)
//          case s if menuType.startsWith(CATEGORIES) => html.addCategory(VintageDesignForms.categoryForm)
//          case s if menuType.startsWith(ORDER_MIRRORS) => html.addOrderMirror(VintageDesignForms.orderMirrorForm)
//          case s if menuType.startsWith(MIRRORS) => html.addMirror(VintageDesignForms.mirrorForm)
        }
      }
    }
  }

  def testOpinion(): Action[AnyContent] = Action { implicit request => Ok {
    html.addOpinion(VintageDesignForms.opinionForm)
  }
  }

  def postOpinion = Action(parse.multipartFormData) { request => {
    val name: String = request.body.dataParts.get("name").get(0)
    val text: String = request.body.dataParts.get("text").get(0)
    var photo: String = ""


    for (file <- request.body.files) {
      val name = file.filename
      val newFile: File = new File("public/images/opinion/" + name)
      file.ref.moveTo(newFile)
      photo = name
    }

    val opinion: Opinion = new Opinion(name, text, photo, 0)
    val insert: Future[Int] = OpinionDao.insert(opinion)
    insert.map {
      result => {

      }
    }.recover {
      case ex: TimeoutException =>
        Logger.error("Problem found in opinion add process")
        InternalServerError(ex.getMessage)
    }

    Redirect(routes.AdminVintageDesign.index)
  }
  }
}
