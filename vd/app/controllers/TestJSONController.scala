package controllers

import play.api.mvc.{AnyContent, Action, Controller}
import play.api.libs.json.Json
import org.postgresql.util._
import play.api.Play.current
import play.api.db.DB
import anorm._

/**
 * Created by Sergey on 15.07.2015.
 */
object TestJSONController extends Controller {
  def write(): Action[AnyContent] = Action {
    implicit request => {
      val json = Json.obj("photos" -> Json.arr("eglo1.jpg", "eglo2.jpg","eglo3.jpg","eglo4.jpg","eglo5.jpg"))
      val pgObject = new PGobject()
      pgObject.setType("json")
      pgObject.setValue(json.toString)
      val sql = SQL("insert into jsontest values (3,{test})").on("test" -> anorm.Object(pgObject))
      DB.withConnection(implicit c => sql.execute())
      Ok("Wrote")
    }
  }
}
