package models

import slick.driver.PostgresDriver.api._
import play.api.db.DB
import play.api.Play.current
import scala.concurrent.Future


/**
 * Created by Sergey on 15.08.2015.
 */
object ArticlesDao {
  val articles = TableQuery[Articles]

  private def db: Database = Database.forDataSource(DB.getDataSource())

  def insert(article: Article): Future[Int] =
    try db.run(articles += article)
    finally db.close

  def delete(name:String): Future[Int] =
    try db.run(articles.filter(_.name === name).delete)
    finally db.close

  def findById(id:Long):Future[Article] =
    try db.run(articles.filter(_.id === id).result.head)
    finally db.close

  def list(): Future[Seq[Article]] = {
    try {
      val query = (for {article <- articles} yield (article)).sortBy(_.id)
      db.run(query.result)
    } finally {
      db.close()
    }
  }
}
