package views.form

import views.html.form._
import views.html.helper.FieldConstructor


/**
 * Created by Sergey on 12.07.2015.
 */
object ModalOrderFieldConstructorHelper {
  implicit val default = FieldConstructor(modalOrderFormFieldConstructor.f)
  val requiredField = FieldConstructor(modalOrderFormRequiredFieldConstructor.f)
  val textAreaConstructor = FieldConstructor(modalOrderFormTextAreaConstructor.f)

}
