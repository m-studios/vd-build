package views.form

import views.html.form._
import views.html.helper.FieldConstructor


/**
 * Created by Sergey on 12.07.2015.
 */
object AdminAddItemFieldConstructorHelper {
  implicit val default = FieldConstructor(adminAddItemFormRequiredFieldConstructor.f)
  val textAreaConstructor = FieldConstructor(adminAddItemFormTextAreaFieldConstructor.f)
}
