import com.mohiva.play.htmlcompressor.HTMLCompressorFilter
import play.api._
import play.api.mvc.WithFilters
import play.filters.gzip.GzipFilter

object Global extends WithFilters(new GzipFilter(), HTMLCompressorFilter()) with GlobalSettings {

  override def onStart(app: Application) {
  }

}

