package bifico.core.repo

trait Repository {
  def openBranch(name:String, fromBranch:String, fromCommit:String)
  def closeBranch(branchIndex:String)

  def merge
}
